[[_TOC_]]

# Latihan 12: Git
Latihan ini adalah untuk menunjukkan pemahaman menggunakan **Git** menggunakan **GitLab**

# Langkah 1.0: Pendaftaran GitLab
Latihan ini adalah untuk mewujudkan akaun GitLab
*  Layari laman **https://gitlab.com/** dan buat pendaftaran baru - ***Get free trial*** dan pilih ***GitLab SaaS***

<img src="https://code.cloud-connect.asia/Hanafiah/pengenalan-proses-moden/uploads/68474f6f805319c659decb9fbf1f0899/image.png" width="600">

# Langkah 2.0: *Git client installation*
*  Muat turun Git client dari https://git-scm.com/downloads
*  Setelah siap, buka aplikasi Git CMD, dan taip kod berikut untuk melihat versi aplikasi git:

```
> git --version
```

# Langkah 3.0: Mewujudkan Projek GitLab
Langkah ini adalah untuk mewujudkan projek baru di GitLab

*  Di laman GitLab anda, klik ***New Project*** untuk mewujudkan projek baru

<img src="https://code.cloud-connect.asia/researchproject/continues-integration-and-delivery/documents/uploads/08a861a8ead1767d38880ca807139f86/image.png" width="150">

<hr>

*  Di paparan ***Create new project***, pilih ***Create blank project***

<img src="https://code.cloud-connect.asia/researchproject/continues-integration-and-delivery/documents/uploads/86a69815554a82c32ebcc729cd7ae848/image.png" width="500">

<hr>

*  Masukkan **Latihan10** untuk **Project name** dan masukkan maklumat projek untuk **Project description**, pilih ***Private*** untuk ***Visibility Level***  dan pastikan **UNCHECKED** untuk **Initialize repository with a README**  seperti dipaparan berikut dan klik ***Create project*** 

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/fa4542a34e5a27ec1d10d6505db52c0c/image.png">

<hr>

*  Berikut adalah paparan setelah berjaya

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/826cb30d472bcae257ebc264b87a6729/image.png">

<hr>

# Langkah 4.0: Muatnaik Projek ke GitLab
Langkah ini adalah untuk muatnaik projek ke GitLab

* Di aplikasi **Command Prompt** untuk **Windows** atau Terminal untuk **MacOS**, buka direktori projek **latihan-10**

```
> cd latihan-10
```

* Taip kod berikut untuk membuka aplikasi latihan-10 menggunakan Visual Studio Code

```
> code .
```

## Langkah 4.1: _Initialize Git_

* Buka terminal di Visual Studio Code, dari **Menu klik Terminal -> New Terminal**, dan taip kod seperti berikut

```
> git init 
```
* jika berjaya, taip kod berikut

```
> git remote add origin git@gitlab.com:hanafiahh/latihan10.git
```

* jika berjaya, taip kod berikut

```
> git add .
```

* jika berjaya, taip kod berikut

```
> git commit -m "Initial commit"
```

* akhir sekali jika berjaya, taip kod berikut

```
> git push -u origin master
```

* Buka GitLab dari browser dan pergi ke laman projek yang diwujudkan dari Langkah 3.0. *Refresh page* dan sahkan fail dan direktori untuk latihan-10 tersenarai

## Langkah 4.2: _Synchronize code repo with GitLab_
Langkah ini adalah untuk muatnaik perubahan kod dan _synchronize code repository_ dengan GitLab

* Wujudkan fail **Cuba.vue** di bawah direktori **views**. Right-click di direktori **views**, klik **New File** 

* Salin dan tampal kod berikut

```
<template>
    <div>
        Testing
    </div>
</template>
```

* Simpan / (Save) fail **Cuba.vue**.

* Di terminal Visual Studio Code, taip kod berikut

```
> git pull
```

* jika berjaya, taip kod berikut

```
> git add .
```

* jika berjaya, taip kod berikut

```
> git commit -m "Initial commit"
```

* akhir sekali jika berjaya, taip kod berikut

```
> git push
```

* Buka GitLab dari browser dan pergi ke laman projek yang diwujudkan dari Langkah 3.0. *Refresh page* dan sahkan fail **Cuba.vue** di direktori **views**


# Langkah 5.0: _Clone_ Projek dari GitLab
Langkah ini adalah untuk _**clone**_ projek dari GitLab

* Di aplikasi **Command Prompt**, wujudkan direktori baru - latihan-git

```
> mkdir latihan-git
> cd latihan-git
```

* Di **internet browser**, layari pautan berikut:

```

```

* Klik **Clone -> Copy URL** untuk **Clone with HTTPS**, seperti paparan berikut

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/3cc7cad578d0576053cb8f969d909ef9/image.png">

* Di aplikasi **Command Prompt**, tampal **URL** yang di salin di kod berikut

```
> clone
```

* Setelah selesai, taip kod berikut

```
> cd 
```
* Taip kod berikut

```
> npm install
```

* Setelah selesai, taip kod berikut untuk mulakan aplikasi

```
> npm run dev 
```
