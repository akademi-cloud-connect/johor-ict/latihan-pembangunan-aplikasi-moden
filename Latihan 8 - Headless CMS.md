[[_TOC_]]

# Latihan 8: Headless CMS
Latihan ini adalah untuk menunjukkan pemahaman membangunkan **Headless CMS** menggunakan platform **Strapi**. 

# Langkah 1.0: Penyediaan Persekitaran Projek
*  Di aplikasi ***Command Prompt*** untuk Windows atau ***Terminal*** untuk MacOS
*  Wujudkan direktori baru - latihan-8

```
> mkdir latihan-8
> cd latihan-8
```

# Langkah 2.0: _**Install**_ Strapi
Langkah ini adalah untuk _install Strapi_ sebagai langkah pertama untuk pembangunan Headless CMS

*  Di ***Command Prompt*** taip kod seperti berikut

```
> npx create-strapi-app headless-cms
```
* Pilih **Quickstart (recommended)**, untuk soalan berikut

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/0945989446dd1cc2ad16ae2158ec9949/image.png" width=600>

* Pilih **n**, untuk soalan berikut

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/ab5bdc0e9b2ad6dab57185e8197f2016/image.png" width=600>

* Setelah selesai, secara automatik internet browser akan dibuka ke pautan http://localhost:1337/admin untuk langkah pendaftaran Strapi, masukkan maklumat yang diperlukan, contohnya seperti paparan berikut

**Nota:**
**Email dan password** akan digunakan untuk akses ke aplikasi Strapi

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/936f94ca42baf3fea2a676430a2052a9/image.png" width=400>

* Klik **LET'S START**, berikut adalah paparan jika berjaya 

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/98b4ce2942b4a68a4583d6c56159e77d/image.png">

# Langkah 3.0: Wujudkan _**Data Structure / Schema**_ untuk Pengumuman
Langkah ini adalah untuk mewujudkan **Data Structure / Schema** seperti database **Tables** dan **Fields** menggunakan **Collection types**

**Nota:**
_**Collection types are content-types that can manage several entries.**_

* Klik **CREATE YOUR FIRST CONTENT-TYPE** seperti paparan berikut

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/d274ce0275482014b742136c12a1c980/image.png" width=400>

* Di paparan berikut masukkan **pengumuman** untuk **Display name** seperti paparan berikut dan klik **Continue**

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/3627e92053136bd96ae1e9bdbefc9310/image.png" width=700>

* Di paparan berikut pilih **Text** dari senarai **Select a field for your collection type**

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/51d2847306286651230417717b8247aa/image.png" width=700>

* Di paparan berikut masukkan **tajuk** untuk **Name**, pilih **Short Text** untuk **Type**, dan klik **Add another field**

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/e8aad41bb17b92634411735bf51717e3/image.png" width=700>

* Pilih **Text** dari senarai **Select a field for your collection type**
* Di paparan berikut masukkan **maklumat** untuk **Name**, pilih **Long Text** untuk **Type**, dan klik **Add another field**

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/6dace9c022d079177839e0b5a4941e16/image.png" width=700>

* Pilih **Media** dari senarai **Select a field for your collection type**
* Di paparan berikut masukkan **photo** untuk **Name**, pilih **Single media** untuk **Type**, dan klik **Finish**

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/92c7e4e8902eac2b992bab9e3b56342b/image.png" width=700>

* Jika berjaya, rujuk paparan berikut dan klik **Save** untuk sahkan maklumat

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/b9f741ae6a3fafa889f71410fa7f2dd8/image.png" width=700>

# Langkah 4.0: Masukkan data Pengumuman
Langkah ini adalah untuk masukkan data Pengumuman

* Dari senarai menu di kiri - **COLLECTION TYPES** seperti di paparan berikut dan klik **Pengumumen** 

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/1aa97ce6d3fdb31065189cf21d24dfde/image.png" width=300>

* Klik **Add New Pengumumen** seperti di paparan berikut

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/7500188d8d06a95cdf834d3fdeaebf99/image.png" width=700>

* Masukkan maklumat seperti contoh berikut, **Latihan Pembangunan Aplikasi Moden** untuk **Tajuk**, **Latihan ini adalah untuk mempelajari pembangunan aplikasi menggunakan teknologi moden** untuk **Maklumat**, pilih gambar yang sesuai untuk **Photo**. Klik **Save** dan seterusnya klik **Publish** untuk sahkan kemasukan data

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/4d21e3d3802ddd66ddc8d2751298e9a5/image.png" width=700>

* Dari senarai menu di kiri - **COLLECTION TYPES** seperti di paparan berikut dan klik **Pengumumen** sekali lagi untuk pastikan data

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/c641268f1a4da853cb0884c959fb92a5/image.png" width=800>

# Langkah 5.0: Henti dan Mula aplikasi Strapi
Langkah ini adalah untuk memahami kaedah untuk **Stop & Start** aplikasi Strapi

## Langkah 5.1: Hentikan aplikasi Strapi

* Di ***Command Prompt***, untuk hentikan applikasi Strapi, tekan kekunci **_Ctrl-C_**

## Langkah 5.2: Mulakan aplikasi Strapi

* Pastikan berada di direktori latihan-8, pergi ke direktori aplikasi Strapi

```
> cd headless-cms
```

* Taip kod berikut untuk mulakan aplikasi Strapi

```
> npm run develop
```
* Jika berjaya, rujuk paparan berikut

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/0dfe0e96fb41aaedfb081e5985fa23ca/image.png" width=700>

* Buka internet browser dan layari pautan berikut http://localhost:1337, masukkan **Email** dan **Password** seperti maklumat semasa pendaftaran di [Langkah 2.0.](#langkah-20-install-strapi)

# Langkah 6.0: Wujudkan API Documentation
Langkah ini adalah untuk wujudkan API Documentation mengikut standard SWAGGER / OpenAPI Specification (OAS) dengan **Install API Documentation Plugins**

## Langkah 6.1: Install API Documentation Plugin

* Di ***Command Prompt***, hentikan applikasi Strapi dahulu dengan tekan kekunci **_Ctrl-C_**. Pastikan berada di direktori aplikasi Strapi

* Taip kod seperti berikut untuk **Install plugins**

```
> npm run strapi install documentation

```
* Berikut adalah paparan jika berjaya

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/c4f840b61bad500a36ea14cefdefc718/image.png" width=700>

## Langkah 6.2: Semak API Documentation

* Mulakan aplikasi Strapi seperti [Langkah 5.2](#langkah-52-mulakan-aplikasi-strapi) 

* Pastikan **Documentation** di senarai **PLUGINS** menu di kiri seperti paparan berikut.

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/0cabc1a280e9a9c74abfbe8f529c0167/image.png" width=300>

* Klik **Documentation** untuk ke paparan seperti berikut

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/2ec2de81e3469dbc34d28aba64dc3e5a/image.png" width=700>

* Klik **Open the documentation** untuk paparan seperti berikut

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/2ec2de81e3469dbc34d28aba64dc3e5a/image.png" width=700>

* Klik **Pengumuman** untuk paparan seperti berikut

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/97a125b6a31fa83b5671ccad884d18e0/image.png">

# Langkah 7.0: Menetapkan _Access Permission_ Strapi API
Langkah ini adalah untuk menetapkan kebenaran utk akses Strapi API yang aboleh diakses oleh aplikasi _Front-end_

## Langkah 7.1: Wujudkan pengguna baru

* Di portal aplikasi Strapi, klik **Users** di senarai **COLLECTION TYPES** menu di kiri seperti paparan berikut

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/ca7ba7d6ddf8bd5ddd3e067ad1c6d05b/image.png" width=300>

* Klik **Add New Users** seperti di paparan berikut

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/2f92a869c59368e2b9b12f54afadafab/image.png" >

* Masukkan **pentadbir** untuk **Username**, **pentadbir@cloud-connect.asia** untuk **Email**, **pentadbir123** untuk **Password**, pilih **ON** untuk **Confirmed**, pilih **Authenticated** untuk **Role** dan klik **Save**

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/4baa26714e9046ff92bf74530add2e14/image.png">

* Klik **Users** di senarai **COLLECTION TYPES** menu di kiri dan sahkan **pentadbir** tersenarai.

## Langkah 7.2: Tetapkan Kebenaran Akses

* Di portal aplikasi Strapi, klik **Settings** di senarai **GENERAL** menu di kiri seperti paparan berikut

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/ca4ab79537a48d5354aeb4f56d9210b6/image.png" width=300>

* Klik **Roles** di senarai **USERS & PERMISSIONS PLUGIN** seperti paparan berikut. Klik **Authenticated** di senarai **Roles**

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/f790045deda3d802df3ef045bfb22ef7/image.png">

*  Klik **Select all** untuk **PENGUMUMAN** di senarai **Permissions** seperti paparan berikut. Klik **Save**

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/2b28ce640550fd578a0157db75bc8898/image.png">

# Langkah 8.0: Uji Akses Strapi API
Langkah ini adalah untuk menguji akses ke Strapi API menggunakan JWT

## Langkah 8.1: Dapatkan JWT Strapi API

*  Di ***Command Prompt***, pastikan berada di direktori latihan-8, dan taip kod seperti berikut untuk buka aplikasi Visual Studio Code

```
> code .
```

* Di aplikasi Visual Studio Code, wujudkan fail **uji.http**. Dari Menu, klik **_File -> New File_**. Salin dan tampal kod berikut

```
// untuk dapatkan Strapi JWT
###
POST http://localhost:1337/auth/local
content-type: application/json

{
    "identifier": "pentadbir",
    "password": "pentadbir123"
    
}
```

* Simpan / (_**Save**_) fail.

* Klik **Send Request**. Sila rujuk paparan berikut:

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/8cc891dbbc49d244c75944eea133524d/image.png">

* Berikut adalah paparan di sebelah kanan jika berjaya dan sahkan maklumat **jwt** dari senarai JSON data. Data JWT ini akan digunakan untuk langkah seterusnya

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/56fdffa241d65907af21a3710961fe3d/image.png">

## Langkah 8.2: Akses Strapi API

* Di fail **uji.http**. Tambah di hujung fail dengan salin dan tampal kod berikut

```
// untuk aksess pengumuman API
###
http://localhost:1337/pengumumen
Content-Type: application/json
Authorization: Bearer 
```

* Salin data **jwt** dari Langkah 8.1, dan tampal ke hujung **Authorization: Bearer** seperti paparan berikut (data jwt akan berbeza mengikut keputusan Langkah 8.1)

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/ff6a44a352d7c8031796683e38c5ddf3/image.png">


* Klik **Send Request**. Berikut adalah sebahagian paparan di sebelah kanan jika berjaya dan sahkan maklumat

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/391cacf75f3aabacaa77d9af6e202edd/image.png">

* Di ***Command Prompt***, untuk hentikan applikasi Strapi, tekan kekunci **_Ctrl-C_**
