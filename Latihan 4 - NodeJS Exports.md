[[_TOC_]]


# Latihan 4: NodeJS Exports

Latihan ini adalah untuk menunjukkan pemahaman fungsi _NodeJS Exports_

# Langkah 1.0: Penggunaan _module.exports_
*  Di aplikasi ***Command Prompt*** untuk Windows atau ***Terminal*** untuk MacOS
*  Wujudkan direktori baru - latihan-4

```
> mkdir latihan-4
> cd latihan-4
```
* Taip kod berikut untuk membuka aplikasi Visual Studio Code

```
> code .
```
* Wujudkan fail **bilang.js**. Dari Menu, klik **_File -> New File_**
* Salin dan tampal data berikut dan 

```
class bilang {
    constructor(a, b) {
        this.a = a;
        this.b = b;
    }

    tambah(a,b){
        let jawapan = this.a + this.b
        return jawapan
    }

    tolak(a,b){
        let jawapan = this.a - this.b
        return jawapan
    }
}
module.exports = bilang
```

* Simpan / (_**Save**_) fail

* Wujudkan fail **index.js**. Dari Menu, klik **_File -> New File_**
* Salin dan tampal data berikut dan 

```
const bilang = require('./bilang.js');

const a = 60
const b = 40
const kira = new bilang(a,b);
  
console.log('Tambah: ' + a + ' + ' + b + ' = ' + kira.tambah())
console.log('Tolak: ' + a + ' - ' + b + ' = ' + kira.tolak())
```
* Simpan / (_**Save**_) fail

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/fc49fd296ff589478d4e8f11fe1f5d17/image.png">

# Langkah 2.0: Uji _module.exports_

*  Buka terminal, dari Menu klik _**Terminal -> New Terminal**_ 

*  Di terminal taip kod seperti berikut

```
> node index.js
```
atau 

```
> node .
```

* Pastikan paparan di terminal adalah seperti berikut:
```
Tambah: 60 + 40 = 100
Tolak: 60 - 40 = 20
```

# Langkah 3.0: Penggunaan _exports_

* Wujudkan fail **congak.js**. Dari Menu, klik **_File -> New File_**

* Salin dan tampal kod berikut

```
exports.tambah = (a,b) => {
    let jawapan = a + b
    return jawapan
}

exports.tolak = (a,b) => {
    let jawapan = a - b
    return jawapan
}
```
* Simpan / (_**Save**_) fail
* Di fail **index.js**, padam kod sedia ada, salin dan tampal kod berikut 

```
const congak = require('./congak.js');

const a = 60
const b = 40
  
console.log('Tambah: ' + a + ' + ' + b + ' = ' + congak.tambah(a,b))
console.log('Tolak: ' + a + ' - ' + b + ' = ' + congak.tolak(a,b))
```
* Simpan / (_**Save**_) fail

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/44429eb1ad59699e2ee8d362efb9fcf9/image.png">

# Langkah 3.0: Uji _exports_

*  Di terminal Visual Studio Code, taip kod seperti berikut

```
> node index.js
```
atau 

```
> node .
```

* Pastikan paparan di terminal adalah seperti berikut:
```
Tambah: 60 + 40 = 100
Tolak: 60 - 40 = 20
```
