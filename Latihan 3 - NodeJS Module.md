[[_TOC_]]


# Latihan 3: NodeJS Module

Latihan ini adalah untuk menunjukkan pemahaman NodeJS File System Module 

# Langkah 1.0: Wujudkan daerah.txt
*  Di aplikasi ***Command Prompt*** untuk Windows atau ***Terminal*** untuk MacOS
*  Wujudkan direktori baru - latihan-3

```
> mkdir latihan-3
> cd latihan-3
```
* Taip kod berikut untuk membuka aplikasi Visual Studio Code

```
> code .
```
* Wujudkan fail **daerah.txt**. Dari Menu, klik **_File -> New File_**
* Salin dan tampal data berikut dan 

```
johor bahru
kota tinggi
segamat
```
* Simpan / (_**Save**_) fail
nota: gunakan CTRL-S untuk simpan fail

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/3683aa6e4329a54d01b07afb371dfd04/image.png" width=600>

# Langkah 2.0: Penggunaan Synchronous File System

* Wujudkan fail **index.js**. Dari Menu, klik **_File -> New File_**

* Salin dan tampal kod berikut

```
// include File System module / library
const fs = require('fs')

// Synchronous reading file daerah.txt content with utf8 encoding
const txt = fs.readFileSync('./daerah.txt','utf8')

// display the file content
console.log(txt)

// display message
console.log('Latihan NodeJS')
```
* Simpan / (_**Save**_) fail

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/be12787d20a56cc7570995457d39df34/image.png">

# Langkah 3.0: Uji Synchronous File System
*  Dari Menu Visual Studio Code buka terminal seperti paparan berikut

<img src="https://code.cloud-connect.asia/Hanafiah/pengenalan-aplikasi-moden/uploads/f02cbf2409449b5d461dc7737676a88b/image.png" width="300">

*  Di terminal taip kod seperti berikut

```
> node index.js
```
atau 

```
> node .
```

* Pastikan paparan di terminal adalah seperti berikut:

```
johor bahru
kota tinggi
segamat
Latihan NodeJS
```

# Langkah 4.0: Penggunaan Asynchronous File System
*  Padam semua kod di fail _**index.js**_, Salin dan tampal kod berikut

```
// include File System module / library
const fs = require('fs')

// Asynchronous reading file daerah.txt content with utf8 encoding
fs.readFile('./daerah.tx','utf8', (err, txt) => {
    // error checking
    if (err) {
        // display error message
        console.log(err)
    }
    else {
        // display the file content
        console.log(txt)
    }
})

// display message
console.log('Latihan NodeJS')
```

*  Simpan / **Save** fail index.js

# Langkah 5.0: Uji Asynchronous File System

*  Di terminal taip kod seperti berikut

```
> node index.js
```
* Pastikan paparan di terminal adalah seperti berikut:

```
Latihan NodeJS
johor bahru
kota tinggi
segamat
```
