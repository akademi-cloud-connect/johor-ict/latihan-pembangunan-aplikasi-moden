[[_TOC_]]

# Latihan 7: Pembangunan API dari MySQL Database yang baru
Latihan ini adalah untuk menunjukkan pemahaman membangunkan MySQL API dari MySQL Database yang baru menggunakan **_PRISMA ORM and ExpressJS Framework_**. Untuk latihan ini MySQL database server akan disediakan di persekitaran pengkomputeran awan. 

**Nota:**
Jika hendak menyediakan persekitaran MySQL database untuk di komputer anda, sila rukjuk pautan berikut untuk maklumat lebih lanjut: https://dev.mysql.com/doc/mysql-installation-excerpt/8.0/en/windows-installation.html

# Langkah 1.0: Penyediaan Persekitaran Projek
*  Di aplikasi ***Command Prompt*** untuk Windows atau ***Terminal*** untuk MacOS
*  Wujudkan direktori baru - latihan-7

```
> mkdir latihan-7
> cd latihan-7
```
* Taip kod berikut untuk membuka aplikasi Visual Studio Code

```
> code .
```

# Langkah 2.0: _**Initialize**_ Persekitaran NodeJS
Langkah ini adalah untuk _initialize NodeJS environment_ sebagai langkah pertama untuk pembangunan API

*  Buka terminal di Visual Studio Code, dari Menu klik _**Terminal -> New Terminal**_ 
*  Di terminal taip kod seperti berikut

```
> npm init -y
```
* Pastikan fail **package.json** wujud.

# Langkah 3.0: _Install & Initialize_ Persekitaran Prisma
Langkah ini adalah untuk _install and initialize Prisma environment_ sebagai langkah untuk pembangunan MSQL API

* Di terminal di Visual Studio Code, taip kod seperti berikut untuk _**install Prisma Client**_

```
> npm install prisma --save-dev
```

* Setelah selesai, di fail **package.json**, pastikan **prisma** adalah salah satu **devDependencies**. Sila rujuk paparan berikut:

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/e4be1690cb86c36916539850e324600d/image.png" width=600>

* Di terminal di Visual Studio Code, taip kod seperti berikut untuk _**initialize Prisma environment**_

```
> npx prisma init
```

* Jika berjaya, sila rujuk paparan berikut:

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/3bdf4cc9d4a20ea0ae61ddcd8157ed3b/image.png" width=600>

* Pastikan direktori **prisma**, fail **.env** dan **prisma/schema.prisma** diwujudkan. Sila rujuk paparan berikut:

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/f71a60de65c049480606cac8be905a18/image.png" width=200>

# Langkah 4.0: _Connecting to MySQL Database using Prisma_
Langkah ini adalah untuk membuat _connection_ ke MSQL database

* Buka fail **.env**, tukar maklumat **DATABASE_URL** dengan salin dan tampal kod seperti berikut.

**Penting: Sila gunakan nama database yang telah diberikan cth: user1 / user2 / user3 ...**

```
DATABASE_URL="mysql://pentadbir:rahsia@167.71.217.175:3306/user1"
```

```
Nota:
- root : adalah username untuk akses ke MySQL database
- rahsia : adalah katalaulan untuk username root
- 167.71.217.175 : adalah database server URL atau localhost jika dari komputer sendiri
- 3306 : adalah port untuk MySQL server
- user1 : adalah nama database
```

* Simpan / (Save) fail.

* Buka fail **schema.prisma** di direktori **prisma**, tukar maklumat **datasource db** dengan salin dan tampal kod seperti berikut

```
datasource db {
  provider = "mysql"
  url      = env("DATABASE_URL")
}
```

* Simpan / (Save) fail. Sila rujuk paparan berikut:

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/422b883caed812c562d867cf04a5808d/image.png" width=600>

# Langkah 5.0: Penggunaan _Prisma Schema_
Langkah ini adalah untuk menggunakan **Prisma Schema** untuk wujudkan **Table** dan **Fields** di database

* Buka fail **schema.prisma**. Salin dan tampal kod berikut di akhir fail
```
model pengguna {
  id     Int    @id @default(autoincrement()) @db.UnsignedInt
  nama   String @db.VarChar(255) @unique
  email  String @db.VarChar(255)
  alamat String @db.VarChar(255)
  daerah String @db.VarChar(255)
  negeri String @db.VarChar(255)
}
```

* Simpan / (Save) fail. Sila rujuk paparan berikut:

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/426a82fc06960012d5c9e68e8f6ae147/image.png" width=600>

# Langkah 6.0: Penggunaan _Prisma Migrate_
Langkah ini adalah untuk menggunakan **Prisma Migrate** untuk wujudkan **table** di database

* Di terminal di Visual Studio Code, taip kod seperti berikut

```
> npx prisma migrate dev
```
* Masukkan **first migrate** bila ditanya **Enter a name for the new migration:**
* Setelah selesai, sila rujuk paparan berikut:

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/49899e96e6c90a16c3a270e8cccd9eed/image.png" width=600>

* Pastikan direktori **migrations** di wujudkan di bawah direktori **prisma** seperti paparan berikut

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/c1dceff899ef8af3b5b7a8b96b8c6360/image.png" width=300>

* Sila lihat kandungan fail **migration.sql**

# Langkah 7.0: Penggunaan _Prisma Studio_
Langkah ini adalah untuk menggunakan **Prisma Studio** untuk akses MySQL database

* Buka aplikasi Command Prompt untuk Windows atau Terminal untuk MacOS dan pastikan berada di direktori projek **latihan-7**, taip kod seperti berikut

```
> npx prisma studio
```

* Jika berjaya, sila rujuk paparan berikut:

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/54e585920ebc15dd7a30e47066a5b599/image.png" width=600>

* Secara automatik juga, **internet browser** dengan URL http://localhost:5555/ akan dibuka seperti paparan berikut

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/7da9841ad5d0d1ee6485ed03b314bbad/image.png" width=300>

* Untuk melihat data di database. Klik **pengguna** di senarai **All Models**, sila rujuk paparan berikut

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/3c1c040068d585988945c93aec055d25/image.png" width=600>

# Langkah 8.0: _Install ExpressJS Framework, body-parser & Prisma Client_
Langkah ini adalah untuk _Install ExpressJS Framework, body-parser & Prisma Client_
<br>

*  Di terminal taip kod seperti berikut

```
> npm install express @prisma/client body-parser
```

* Setelah selesai, di fail _**package.json**_, pastikan _**express, body-parser, @prisma/client**_ adalah salah satu _**dependencies**_. Sila rujuk paparan berikut:

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/b72b816b1d1c83bae4ba9787e8f57887/image.png" width=500>

# Langkah 9.0: Penggunaan _ExpressJS Framework_
Langkah ini adalah untuk membangunkan API menggunakan ExpressJS Framework
* Di Visual Studio Code, wujudkan fail **index.js**. Dari Menu, klik **_File -> New File_**
* Salin dan tampal kod berikut

```
const express = require('express');
const app = express();

app.get('/', (req, res) => {
    res.send('Salam Muafakat!');
});

app.listen(8080, () => {
    console.log('App listening on port 8080!');
});

//Run app, then load http://localhost:8080 in a browser to see the output.
```
* Simpan / (_**Save**_) fail. Sila rujuk paparan berikut:

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/b55ab9fe0fc002edbaceb2d2703e708e/image.png" width=600>

# Langkah 10.0: Pengunaan NPM RUN
Langkah ini adalah untuk membuat konfigurasi NodeJS dengan menggunakan _command **NPM RUN**_
* Buka fail **package.json**.
* Tukar kod _**"script"**_ dengan salin dan tampal kod seperti berikut

```
"scripts": {
    "start": "node index.js"
  },
```
* Simpan / (_**Save**_) fail. Sila rujuk paparan berikut:

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/aca4f07d1affdece939e96ba4ec28320/image.png" width=300>

# Langkah 11.0: Uji Penggunaan ExpressJS Framework

* Di terminal taip kod seperti berikut:

```
> npm run start
```
* Jika berjaya, berikut adalah paparan maklumat di terminal (maklumat akan berbeza di terminal anda):
```
> latihan-6@1.0.0 start /Users/hanafiah/Documents/Development/node/latihan/latihan-6
> node index.js

App listening on port 8080!
```
* Buka internet browser, dan layari **http://localhost:8080** dengan paparan berikut

```
Salam Muafakat!
```

* Di terminal, untuk **_stop_** applikasi NodeJS, tekan kekunci **_Ctrl-C_**

# Langkah 12.0: _CREATE, READ, UPDATE, DELETE API for MySQL_
Langkah ini adalah untuk membangunkan _CREATE, READ, UPDATE, DELETE API for MySQL_ di **table Pengguna**

* Tukar kod di fail **index.js** dengan salin dan tampal kod berikut 

```
const express = require('express');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();

app.get('/', (req, res) => {
    res.send('Salam Muafakat!');
});

// READ API
app.get('/pengguna', async (req, res) => {
    const jawapan = await prisma.pengguna.findMany();
    res.json(jawapan);
});

// Find data based on nama
app.get('/pengguna/:nama', async (req, res) => {
    let username = req.params.nama
    const jawapan = await prisma.pengguna.findUnique({
        where: {
          nama: username,
        },
    })
    res.json(jawapan);

});

// CREATE / ADD API untuk satu data
app.post('/pengguna', async (req, res) => {
    const jawapan = await prisma.pengguna.create({ data: req.body });
    res.json(jawapan);
});

// CREATE / ADD API untuk banyak data
app.post('/penggunaramai', async (req, res) => {
    const jawapan = await prisma.pengguna.createMany({
        data: req.body,
        skipDuplicates: true, // Skip 'Bobo'
    })
    res.json(jawapan);
});

// Update API guna id
app.put('/pengguna/:userid', async (req, res) => {
    const userid = req.params.userid
    const jawapan = await prisma.pengguna.update({ 
        where: { id : Number(userid) },
        data: req.body
    });
    res.json(jawapan);
});

// Update API guna nama
app.put('/updatepengguna/:nama', async (req, res) => {
    let username = req.params.nama
    console.log(username);
    const jawapan = await prisma.pengguna.update({ 
        where: { nama : String(username) },
        data: req.body
    });
    res.json(jawapan);
});

// DELETE API
app.delete('/pengguna/:userid', async (req, res) => {
    const userid = req.params.userid
    const jawapan = await prisma.pengguna.delete({ 
        where: { id : Number(userid) },
    });
    res.json(jawapan);
});


app.listen(8080, () => {
    console.log('App listening on port 8080!');
});

//Run app, then load http://localhost:8080 in a browser to see the output.
```

* Simpan / (_**Save**_) fail. Sila rujuk paparan berikut:

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/05b9324fd8f94a73174eaa15c041ec5d/image.png" width=600>


# Langkah 13.0: Uji _CREATE, READ, UPDATE, DELETE API for MySQL_
Langkah ini adalah untuk tambah, baca, tukar & buang data pengguna

* Di terminal taip kod seperti berikut:

```
> npm run start
```

*  Wujudkan fail **uji.http**. Dari Menu, klik **_File -> New File_**. Salin dan tampal kod berikut

```
GET http://localhost:8080


// READ API - dapatkan semua data
###
GET http://localhost:8080/pengguna


// CREATE API - tambah satu data
###
POST http://localhost:8080/pengguna
content-type: application/json

{
    "nama": "hanafiahhassan",
    "email": "user@cloud-connect.asia",
    "alamat": "Kg Melayu Kangkar Pulai",
    "daerah": "Johor Bahru",
    "negeri": "Johor"
}

// READ API - dapatkan data mengikut field nama
###
GET http://localhost:8080/pengguna/hanafiahhassan


// CREATE API - tambah banyak data
###
POST http://localhost:8080/penggunaramai
content-type: application/json

[
    {
        "nama": "hanafiah",
        "email": "user@cloud-connect.asia",
        "alamat": "Taman Guru",
        "daerah": "Kota Tinggi",
        "negeri": "Johor"
    },
    {
        "nama": "hassan",
        "email": "user@cloud-connect.asia",
        "alamat": "Kg Melayu Kangkar Pulai",
        "daerah": "Johor Bahru",
        "negeri": "Johor"
    }
]


###
PUT http://localhost:8080/pengguna/19
content-type: application/json

{
    "alamat": "Kg Kelantan"
}


###
DELETE http://localhost:8080/pengguna/1


###
PUT http://localhost:8080/updatepengguna/hassan
content-type: application/json

{
    "alamat": "Kota Kecil",
    "daerah": "Kota Tinggi"
}

```
* Simpan / (_**Save**_) fail.

## Langkah 13.1: Uji _CREATE / Add API for MySQL_

* Di fail uji.http, klik **Send Request**. Untuk **request** berikut

```
// CREATE API - tambah satu data
###
Send Request
POST http://localhost:8080/pengguna
content-type: application/json

{
    "nama": "hanafiahhassan",
    "email": "user@cloud-connect.asia",
    "alamat": "Kg Melayu Kangkar Pulai",
    "daerah": "Johor Bahru",
    "negeri": "Johor"
}
```

## Langkah 13.2: Uji _READ API for MySQL_

* Di fail **uji.http**. Uji kemasukkan data dengan klik **Send Request**. Untuk **request** berikut

```
// READ API - dapatkan semua data
###
Send Request
GET http://localhost:8080/pengguna
```
* Sila pastikan data yang dimasukkan tersenarai dari keputusan dapatan.

## Langkah 13.3: Uji _CREATE / Add API for multiple data_

* Di fail **uji.http**. Uji kemasukkan data dengan klik **Send Request**. Untuk **request** berikut

```
// CREATE API - tambah banyak data
###
Send Request
POST http://localhost:8080/penggunaramai
content-type: application/json

[
    {
        "nama": "hanafiah",
        "email": "user@cloud-connect.asia",
        "alamat": "Taman Guru",
        "daerah": "Kota Tinggi",
        "negeri": "Johor"
    },
    {
        "nama": "hassan",
        "email": "user@cloud-connect.asia",
        "alamat": "Kg Melayu Kangkar Pulai",
        "daerah": "Johor Bahru",
        "negeri": "Johor"
    }
]
```

## Langkah 13.4: Uji _READ API based on nama_

* Di fail **uji.http**. Uji data dengan **request** mengikut nama dengan klik **Send Request**. Untuk **request** berikut

```
// READ API - dapatkan data mengikut field nama
Send Request
###
GET http://localhost:8080/pengguna/hanafiah
```

* Sila pastikan data yang dimasukkan tersenarai dari keputusan dapatan.

## Langkah 13.5: Uji _UPDATE API for MySQL_

* Di fail uji.http, klik **Send Request**. Untuk **request** berikut

```
###
Send Request
PUT http://localhost:8080/pengguna/1
content-type: application/json

{
    "alamat": "Kg Kelantan"
}
```
* Sila pastikan data yang dikemaskini tersenarai dari keputusan dapatan.

## Langkah 13.6: Uji _DELETE API for MySQL_

* * Di fail uji.http, klik **Send Request**. Untuk **request** berikut

```
###
Send Request
DELETE http://localhost:8080/pengguna/1
```

* Semak data yang sudah dipadam dengan klik **Send Request**. Untuk **request** berikut

```
// READ API - dapatkan semua data
###
Send Request
GET http://localhost:8080/pengguna
```
* Sila pastikan data yang dipadam tidak tersenarai dari keputusan dapatan.

* Di terminal, untuk **_stop_** applikasi NodeJS, tekan kekunci **_Ctrl-C_**
