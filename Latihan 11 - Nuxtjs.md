[[_TOC_]]

# Latihan 11: Nuxt.js
Latihan ini adalah untuk menunjukkan pemahaman membangunkan aplikasi web atau website menggunakan **Nuxtjs Framework**. 

# Langkah 1.0: Penyediaan Persekitaran Projek
*  Di aplikasi ***Command Prompt*** untuk Windows atau ***Terminal*** untuk MacOS
*  Wujudkan direktori baru - latihan-11

```
> mkdir latihan-11
> cd latihan-11
```

# Langkah 2.0: Nuxt.js

## Langkah 2.1: _**Install**_ Aplikasi Nuxt.js
Langkah ini adalah untuk wujudkan aplikasi Nuxt.js

* Di Command Prompt taip kod seperti berikut

```
> npx create-nuxt-app nuxtapp
```

* Untuk soalan **Project name** seperti paparan berikut, klik **ENTER**

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/7007d20f3fc7d32c5191c279367444cf/image.png">

* Untuk soalan **Programming language** seperti paparan berikut, pilih **Javascript**, klik **ENTER**

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/83564df76124b43469fb9ce4a6b05f1c/image.png">

* Untuk soalan **Package manager** seperti paparan berikut, pilih **Npm**, klik **ENTER**

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/74717a9eb9e9a4224be9d1ca768a6020/image.png">

* Untuk soalan **UI framework** seperti paparan berikut, pilih **Bootstrap Vue**, klik **ENTER**

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/73ff5f9d8b81182f6513523ef4af47a5/image.png">

* Untuk soalan **Nuxt.js modules** seperti paparan berikut, pilih **Axios** dengan klik **SPACEBAR**, dan klik **ENTER**

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/49b8dd25ddb28ebcec5cfa904f9d1d2d/image.png">

* Untuk soalan **Linting tools** seperti paparan berikut, pilih **Prettier** dengan klik **SPACEBAR**, dan klik **ENTER**

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/706ad44a59a1592876ed1afa33123bc3/image.png">

* Untuk soalan **Testing framework** seperti paparan berikut, pilih **None** dengan klik **SPACEBAR**, dan klik **ENTER**

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/010217ffa7aaa684e1fa3eef76efd569/image.png">

* Untuk soalan **Rendering mode** seperti paparan berikut, pilih **Universal** dengan klik **SPACEBAR**, dan klik **ENTER**

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/3bc86a3bb4751038f87b2be4c73008f3/image.png">

* Untuk soalan **Deployment target** seperti paparan berikut, pilih **Server** dengan klik **SPACEBAR**, dan klik **ENTER**

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/44eb9bb7ad872a993ffcf600278ad0f0/image.png">

* Untuk soalan **Deployment tools** seperti paparan berikut, pilih **jsconfig.json** dengan klik **SPACEBAR**, dan klik **ENTER**

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/cd3541473904f3a14f98dacfe6f38004/image.png">

* Untuk soalan **Continuous integration** seperti paparan berikut, pilih **None** dengan klik **SPACEBAR**, dan klik **ENTER**

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/455fb3f928347e8d4720814a1e4f069b/image.png">

* Untuk soalan **Version control system** seperti paparan berikut, pilih **None** dengan klik **SPACEBAR**, dan klik **ENTER**

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/b9b4fc9d852ac613c7d4ba719627053d/image.png">

* Setelah selesai, taip kod berikut untuk akses ke direktori projek **nuxtapp**

```
> cd nuxtapp
```

* Taip kod berikut untuk membuka aplikasi Visual Studio Code

```
> code .
```

* Buka terminal di Visual Studio Code, dari Menu klik **Terminal -> New Terminal**, dan taip kod seperti berikut untuk mulakan aplikasi **nuxtapp**

```
> npm run dev
```

* Setelah berjaya, buka internet browser dan akses ke pautan http://localhost:3000/ seperti paparan berikut

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/d8db3cacc3b9c0ce473c69c50afde20e/image.png">

## Langkah 2.2: Konfigurasi ikon BoostrapVue 
Langkah ini adalah konfigurasi untuk menggunakan ikon BoostrapVue

* Buka fail **nuxt.config.js**. Tambah kod berikut di hujung fail seperti paparan berikut

```
bootstrapVue: {
    icons: true
  }
```
<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/13c1903fac558ae0ccc83ce7ec16eaeb/image.png" width=550>

* Simpan / (Save) fail **nuxt.config.js**.

## Langkah 2.3: Ubah fail index.vue

* Buka fail **index.vue** di dalam direktori **pages**. Buang kod sediada, salin dan tampal kod berikut

```
<template>
<b-container fluid>
  <b-jumbotron header="Salam Muafakat!" lead="Portal Latihan Aplikasi Moden">
    <p>Untuk maklumat lanjut sila buat pendaftaran</p>
    <b-button variant="primary" href="#">Daftar</b-button>
  </b-jumbotron>
</b-container>
</template>

<script>
export default {
  head: {
      title: "Latihan Nuxt.js"
  },
}
</script>
```

* Simpan / (Save) fail **index.vue**.

* Buka internet browser dan akses ke pautan http://localhost:3000/ seperti paparan berikut

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/808ec64452c624bf3872446207a8a0dc/image.png">

## Langkah 2.4: _Nuxt File System Routing_
Langkah ini adalah untuk menunjukkan pemahaman _Nuxt File System Routing_ sebagai **navigation** ke pautan laman web aplikasi

### Langkah 2.4.1: Wujudkan laman Daftar
* Wujudkan fail **daftar.vue** di bawah direktori **pages**. Right-click di direktori **pages**, klik **New File** 
* Salin dan tampal kod berikut

```
<template>
  <div>
    <b-container>
      <b-row class="justify-content-md-center">
        <b-col md="auto">
        <h1> Pendaftaran </h1>
        <b-form @submit="onSubmit" @reset="onReset" v-if="show">

        <b-form-group 
            id="input-group-1" 
            label="Nama:" 
            label-for="input-1"
        >
            <b-form-input
            id="input-1"
            v-model="form.name"
            placeholder="Masukkan nama anda"
            required
            ></b-form-input>
        </b-form-group>

        <b-form-group
            id="input-group-2"
            label="Emel :"
            label-for="input-2"
            description="Emel mestilah emel yang sah"
        >
            <b-form-input
            id="input-2"
            v-model="form.email"
            type="email"
            placeholder="Masukkan emel anda"
            required
            ></b-form-input>
        </b-form-group>

        <b-form-group id="input-group-3" label="Daerah:" label-for="input-3">
            <b-form-select
            id="input-3"
            v-model="form.daerah"
            :options="daerah"
            required
            ></b-form-select>
        </b-form-group>
        <hr />
        <b-button type="submit" variant="primary">Hantar</b-button>&nbsp;
        <b-button type="reset" variant="danger">Reset</b-button>
        </b-form>
        </b-col>
      </b-row>
    </b-container>
  </div>
</template>

<script>
  export default {
    head: {
        title: "Pendaftaran"
    },
    data() {
      return {
        form: {
          email: '',
          name: '',
          daerah: null,
        },
        daerah: [{ text: 'Pilih daerah', value: null }, 'Johor Bahru', 'Kota Tinggi', 'Segamat'],
        show: true,
      }
    },
    methods: {
      onSubmit(event) {
        event.preventDefault()
        alert(JSON.stringify(this.form))
      },
      onReset(event) {
        event.preventDefault()
        // Reset our form values
        this.form.email = ''
        this.form.name = ''
        this.form.daerah = null
        // Trick to reset/clear native browser form validation state
        this.show = false
        this.$nextTick(() => {
          this.show = true
        })
      }
    }
  }
</script>
```

* Simpan / (Save) fail **daftar.vue**.

* Buka internet browser dan akses ke pautan http://localhost:3000/daftar seperti paparan berikut

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/12ba3c78fe8e73cb73fee58c844c8635/image.png">

### Langkah 2.4.2: Wujudkan laman Login
* Wujudkan fail **login.vue** di bawah direktori **pages**. Right-click di direktori **pages**, klik **New File** 
* Salin dan tampal kod berikut

```
<template>
  <div>
    <b-container>
      <b-row class="justify-content-md-center">
        <b-col md="auto">
        <h1> Login </h1>
        <b-form @submit="onSubmit" @reset="onReset" v-if="show">

        <b-form-group
            id="input-group-1"
            label="Emel :"
            label-for="input-1"
            description="Emel mestilah emel yang sah"
        >
            <b-form-input
            id="input-1"
            v-model="form.emel"
            type="email"
            placeholder="Masukkan emel anda"
            required
            ></b-form-input>
        </b-form-group>
        <b-form-group 
            id="input-group-2" 
            label="Katalaluan:" 
            label-for="input-2"
        >
            <b-form-input
            id="input-2"
            v-model="form.katalaluan"
            type="password"
            placeholder="Masukkan katalaluan anda"
            required
            ></b-form-input>
        </b-form-group>
        <hr />
        <b-button type="submit" variant="primary">Login</b-button>&nbsp;
        <b-button type="reset" variant="danger">Reset</b-button>
        </b-form>
        </b-col>
      </b-row>
    </b-container>
  </div>
</template>

<script>
  export default {
    head: {
        title: "Login"
    },
    data() {
      return {
        form: {
          emel: '',
          katalaluan: '',
        },
        show: true,
      }
    },
    methods: {
      onSubmit(event) {
        event.preventDefault()
        alert(JSON.stringify(this.form))
      },
      onReset(event) {
        event.preventDefault()
        // Reset our form values
        this.form.emel = ''
        this.form.katalaluan = ''
        // Trick to reset/clear native browser form validation state
        this.show = false
        this.$nextTick(() => {
          this.show = true
        })
      }
    }
  }
</script>
```

* Simpan / (Save) fail **login.vue**.

* Buka internet browser dan akses ke pautan http://localhost:3000/login seperti paparan berikut

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/3cf31c058257b36cd8442e2d3cb03b5a/image.png">

## Langkah 2.5: _Nuxt Components_
Langkah ini adalah untuk menunjukkan pemahaman _Nuxt Components_

* Wujudkan fail **NavBar.vue** di bawah direktori **components**. Right-click di direktori **components**.

* Salin dan tampal kod berikut

```
<template>
    <b-container fluid>
        <b-navbar toggleable="lg" type="dark" variant="info">
            <div class="mb-2">
                <b-avatar icon="file-earmark-bar-graph"></b-avatar>
            </div>

            <b-navbar-toggle target="nav-collapse"></b-navbar-toggle>

            <b-collapse id="nav-collapse" is-nav>
            <b-navbar-nav>
                <b-nav-item to="/">Utama</b-nav-item>
                <b-nav-item to="/daftar">Daftar</b-nav-item>
                <b-nav-item to="/login">Login</b-nav-item>
            </b-navbar-nav>
            </b-collapse>
        </b-navbar>
    </b-container>
</template>
```
* Simpan / (Save) fail **NavBar.vue**.

## Langkah 2.6: _Nuxt Layouts_
Langkah ini adalah untuk menunjukkan pemahaman _Nuxt Layouts_

* Wujudkan direktori baru **layouts**

### Langkah 2.6.1: Wujudkan _default layout_

* Wujudkan fail **default.vue** di bawah direktori **layouts**. Right-click di direktori **layouts**, klik **New File** 

* Salin dan tampal kod berikut

```
<template>
    <b-container fluid>
    <NavBar />
    <Nuxt />
    </b-container>
</template>
```
* Simpan / (Save) fail **default.vue**.

* Buka internet browser dan akses ke pautan http://localhost:3000 seperti paparan berikut

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/131a207805c9996a8fb69803915ba578/image.png">

* Klik pautan **Daftar**, **Login** dan sahkan terdapat **NavBar**

### Langkah 2.6.2: Wujudkan _login layout_

* Wujudkan fail **TiadaNavBar.vue** di bawah direktori **layouts**. Right-click di direktori **layouts**, klik **New File** 

* Salin dan tampal kod berikut

```
<template>
    <b-container fluid>
    <Nuxt />
    </b-container>
</template>
```
* Simpan / (Save) fail **TiadaNavBar.vue**.

* Buka fail **login.vue**, buang kod sediada, salin dan tampal kod berikut

```
<template>
  <div>
    <b-container>
      <b-row class="justify-content-md-center">
        <b-col md="auto">
        <h1> Login </h1>
        <b-form @submit="onSubmit" @reset="onReset" v-if="show">

        <b-form-group
            id="input-group-1"
            label="Emel :"
            label-for="input-1"
            description="Emel mestilah emel yang sah"
        >
            <b-form-input
            id="input-1"
            v-model="form.emel"
            type="email"
            placeholder="Masukkan emel anda"
            required
            ></b-form-input>
        </b-form-group>
        <b-form-group 
            id="input-group-2" 
            label="Katalaluan:" 
            label-for="input-2"
        >
            <b-form-input
            id="input-2"
            v-model="form.katalaluan"
            type="password"
            placeholder="Masukkan katalaluan anda"
            required
            ></b-form-input>
        </b-form-group>
        <hr />
        <b-button type="submit" variant="primary">Login</b-button>&nbsp;
        <b-button type="reset" variant="danger">Reset</b-button>
        <hr />
        <NuxtLink to="/">Utama</NuxtLink>
        </b-form>
        </b-col>
      </b-row>
    </b-container>
  </div>
</template>

<script>
  export default {
    head: {
        title: "Login"
    },
    layout: "TiadaNavBar",
    data() {
      return {
        form: {
          emel: '',
          katalaluan: '',
        },
        show: true,
      }
    },
    methods: {
      onSubmit(event) {
        event.preventDefault()
        alert(JSON.stringify(this.form))
      },
      onReset(event) {
        event.preventDefault()
        // Reset our form values
        this.form.emel = ''
        this.form.katalaluan = ''
        // Trick to reset/clear native browser form validation state
        this.show = false
        this.$nextTick(() => {
          this.show = true
        })
      }
    }
  }
</script>

```
* Simpan / (Save) fail **login.vue**.

* Buka internet browser dan akses ke pautan http://localhost:3000. Klik **Login** dan pastikan **Navbar** sudah tiada.

# Langkah 3.0: _Data Fetching_
Langkah ini adalah untuk memahami kaedah mengambil data untuk aplikasi Nuxt.js 

## Langkah 3.1: Penggunaan Axios
Langkah ini adalah untuk memahami penggunaan **Axios** sebagai kaedah mengambil data dari **API endpoints**

### Langkah 3.1.1: Buka projek Latihan 8
Langkah ini akan menggunakan aplikasi yang dibangunkan dari **Latihan 8: Headless CMS** untuk membaca data dari Headless CMS

* Di terminal Visual Studio Code untuk Latihan 8, taip kod berikut untuk mulakan aplikasi Headless CMS

```
> npm run develop
```

### Langkah 3.1.2: Membaca data dari _Strapi Headless CMS_
Langkah ini adalah untuk menggunakan **Axios** untuk membaca data dari **Strapi Headless CMS**

* Wujudkan fail **Pengumuman.vue** di bawah direktori **components**. Right-click di direktori **components**, klik **New File** 

* Salin dan tampal kod berikut

```
<template>
  <div>
    <b-container fluid>
        <b-row cols="1" cols-sm="2" cols-md="3" cols-lg="4">
            <b-col v-for="(item, id) in pengumuman" :key="id">
                <b-card
                :title="item.tajuk"
                :img-src="baseurl + item.photo.url"
                img-alt="Image"
                img-top
                tag="article"
                style="max-width: 20rem;"
                class="mb-2"
                >
                    <b-card-text>
                    {{ item.maklumat }}
                    </b-card-text>

                    <b-button href="#" variant="primary">Lihat Semua</b-button>
                </b-card>
            </b-col>
        </b-row>
    </b-container>
  </div>
</template>

<script>
export default {
    name: 'Pengumuman',
    props: ['pengumuman'],
    data() {
        return {
            baseurl: "http://localhost:1337"
        }
    },
}
</script>
```
* Simpan / (Save) fail **Pengumuman.vue**.

* Buka fail **index.vue** di bawah direktori **pages**. Buang kod sediada, salin dan tampal kod berikut

```
<template>
<b-container fluid>
  <b-jumbotron header="Salam Muafakat!" lead="Portal Latihan Aplikasi Moden">
    <p>Untuk maklumat lanjut sila buat pendaftaran</p>
    <b-button variant="primary" href="#">Daftar</b-button>
  </b-jumbotron>
  <Pengumuman :pengumuman="eventlist" />
</b-container>
</template>

<script>
export default {
  head: {
      title: "Latihan Nuxt.js"
  },
  data () {
    return {
      baseurl: 'http://localhost:1337',
      eventlist: []
    }
  },
  mounted () {
    console.log("start");
    this.getPengumuman()
  },
  methods: {
    getPengumuman() {
      this.$axios.post('http://localhost:1337/auth/local', {
        identifier: 'user1',
        password: 'user123'
      })
      .then((response) => {
          const jwt = response.data.jwt
          this.$axios.setHeader('Authorization', 'Bearer ' + jwt)
          this.$axios.get('http://localhost:1337/pengumumen')
          .then(response => {
            this.eventlist = response.data
            console.log(this.eventlist)
          })
          .catch(error => {
            console.log(error)
          })
      })
      .catch(error => {
          console.log(error)
      })
    }
  },
}
</script>
```

* Simpan / (Save) fail **index.vue**.

### Langkah 3.1.3: Buka projek Latihan 7
Langkah ini akan menggunakan aplikasi yang dibangunkan dari **Latihan 7: - Pembangunan API dari MySQL Database yang baru** untuk membaca data dari MySQL database

* Buka projek Latihan 7 menggunakan Visual Studio code, buka terminal dan taip kod berikut untuk install _**cors**_

```
> npm install cors
```

* Buka fail **index.js**, buang kod sediaada, salin dan tampal kod berikut

```
const express = require('express');
const cors = require("cors");
const bodyParser = require('body-parser');

const app = express();

var corsOptions = {
    origin: "http://localhost:3000"
  };

app.use(cors(corsOptions));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();

app.get('/', (req, res) => {
    res.send('Salam Muafakat!');
});

// READ API
app.get('/pengguna', async (req, res) => {
    const jawapan = await prisma.pengguna.findMany();
    res.json(jawapan);
});

// Find data based on nama
app.get('/pengguna/:nama', async (req, res) => {
    let username = req.params.nama
    const jawapan = await prisma.pengguna.findUnique({
        where: {
          nama: username,
        },
    })
    res.json(jawapan);

});

// Find data based on nama
app.get('/penggunadaerah/:daerah', async (req, res) => {
    let namadaerah = req.params.daerah
    const jawapan = await prisma.pengguna.findMany({
        where: {
          daerah: namadaerah,
        },
    })
    res.json(jawapan);

});

// CREATE / ADD API untuk satu data
app.post('/pengguna', async (req, res) => {
    const jawapan = await prisma.pengguna.create({ data: req.body });
    res.json(jawapan);
});

// CREATE / ADD API untuk banyak data
app.post('/penggunaramai', async (req, res) => {
    const jawapan = await prisma.pengguna.createMany({
        data: req.body,
        skipDuplicates: true, 
    })
    res.json(jawapan);
});

// Update API guna id
app.put('/pengguna/:userid', async (req, res) => {
    const userid = req.params.userid
    const jawapan = await prisma.pengguna.update({ 
        where: { id : Number(userid) },
        data: req.body
    });
    res.json(jawapan);
});

// Update API guna nama
app.put('/updatepengguna/:nama', async (req, res) => {
    let username = req.params.nama
    console.log(username);
    const jawapan = await prisma.pengguna.update({ 
        where: { nama : String(username) },
        data: req.body
    });
    res.json(jawapan);
});

// DELETE API
app.delete('/pengguna/:userid', async (req, res) => {
    const userid = req.params.userid
    const jawapan = await prisma.pengguna.delete({ 
        where: { id : Number(userid) },
    });
    res.json(jawapan);
});


app.listen(8080, () => {
    console.log('App listening on port 8080!');
});

//Run app, then load http://localhost:8080 in a browser to see the output.

```

* Simpan / (Save) fail **index.js**

* Di terminal Visual Studio Code untuk Latihan 7, taip kod berikut untuk mulakan aplikasi Latihan 7

```
> npm run start
```

### Langkah 3.1.4: Membaca data dari MySQL API
Langkah ini adalah untuk menggunakan **Axios** untuk membaca data dari **MySQL API endpoints**

* Kembali semula ke Latihan 11

* Wujudkan fail **pengguna.vue** di bawah direktori **pages**. Right-click di direktori **pages**, klik **New File** 

* Salin dan tampal kod berikut

```
<template>
    <b-container fluid>
        <br>
        <b-row>
            <b-col>
                <h2>Senarai Pengguna</h2>
            </b-col>
        </b-row>
        <b-row>
            <b-col>
                <b-table striped hover :items="penggunalist"></b-table>
            </b-col>
        </b-row>
    </b-container>
</template>

<script>
    export default {
        head: {
            title: "Pengguna"
        },
        data() {
            return {
                penggunalist: [],
            }
        },
        mounted () {
            this.getPengguna();
        },
        methods: {
            getPengguna() {
                this.$axios.get('http://localhost:8080/pengguna')
                .then(response => {
                    this.penggunalist = response.data
                    console.log(this.penggunalist)
                })
                .catch(error => {
                    console.log(error)
                })
            }
        },
    }

</script>

<style lang="scss" scoped>

</style>
```
* Simpan / (Save) fail **pengguna.vue**.

* Buka fail **NavBar.vue** di bawah direktori **components**.

* Padam kod sediaada, Salin dan tampal kod berikut

```
<template>
    <b-container fluid>
        <b-navbar toggleable="lg" type="dark" variant="info">
            <div class="mb-2">
                <b-avatar icon="file-earmark-bar-graph"></b-avatar>
            </div>

            <b-navbar-toggle target="nav-collapse"></b-navbar-toggle>

            <b-collapse id="nav-collapse" is-nav>
            <b-navbar-nav>
                <b-nav-item to="/">Utama</b-nav-item>
                <b-nav-item to="/daftar">Daftar</b-nav-item>
                <b-nav-item to="/pengguna">Pengguna</b-nav-item>
                <b-nav-item to="/login">Login</b-nav-item>
            </b-navbar-nav>
            </b-collapse>
        </b-navbar>
    </b-container>
</template>
```
* Simpan / (Save) fail **NavBar.vue**.

* Buka internet browser dan akses ke pautan http://localhost:3000. Klik pautan **Pengguna** untuk paparan seperti berikut. Nota: Data mungkin berbeza

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/292ef9f4ae8cb5fafe0787b1212c57ac/image.png">

### Langkah 3.1.5: Masukkan data melalui MySQL API
Langkah ini adalah untuk menggunakan **Axios** untuk memasukkan data melalui **MySQL API endpoints**

* Sebelum mulakan langkah ini, pastikan aplikasi **MySQL API** untuk **Latihan 7 sudah dimulakan**

* Buka fail **daftar.vue** di bawah direktori **pages**.

* Buang kod sediada, salin dan tampal kod berikut

```
<template>
  <div>
    <b-container>
      <b-row class="justify-content-md-center">
        <b-col md="auto">
        <h1> Pendaftaran </h1>
        <b-form @submit="onSubmit" @reset="onReset" v-if="show">

        <b-form-group id="input-group-1" label="Nama:" label-for="input-1">
            <b-form-input
            id="input-1"
            v-model="form.nama"
            placeholder="Masukkan nama anda"
            required
            ></b-form-input>
        </b-form-group>

        <b-form-group id="input-group-2" label="Emel :" label-for="input-2">
            <b-form-input
            id="input-2"
            v-model="form.email"
            type="email"
            placeholder="Masukkan emel anda"
            required
            ></b-form-input>
        </b-form-group>

        <b-form-group id="input-group-3" label="Alamat:" label-for="input-3">
            <b-form-input
            id="input-3"
            v-model="form.alamat"
            placeholder="Masukkan alamat anda"
            required
            ></b-form-input>
        </b-form-group>

        <b-form-group id="input-group-4" label="Daerah:" label-for="input-4">
            <b-form-select
            id="input-4"
            v-model="form.daerah"
            :options="daerah"
            required
            ></b-form-select>
        </b-form-group>

        <b-form-group id="input-group-5" label="Negeri:" label-for="input-5">
            <b-form-input
            id="input-3"
            v-model="form.negeri"
            placeholder="Masukkan negeri anda"
            required
            ></b-form-input>
        </b-form-group>
        <hr />
        <b-button type="submit" variant="primary">Hantar</b-button>&nbsp;
        <b-button type="reset" variant="danger">Reset</b-button>
        </b-form>
        <br>
        <span>{{ jawapan }}</span>
        </b-col>
      </b-row>
    </b-container>
  </div>
</template>

<script>
  export default {
    head: {
        title: "Pendaftaran"
    },
    data() {
      return {
        form: {
          nama: '',
          email: '',
          alamat: '',
          daerah: null,
          negeri: ''
        },
        daerah: [{ text: 'Pilih daerah', value: null }, 'Johor Bahru', 'Kota Tinggi', 'Segamat'],
        show: true,
        jawapan: ''
      }
    },
    methods: {
      onSubmit(event) {
        event.preventDefault()
        this.createAPI(this.form)
        //alert(JSON.stringify(this.form))
      },
      onReset(event) {
        event.preventDefault()
        // Reset our form values
        this.form.nama = ''
        this.form.email = ''
        this.form.alamat = ''
        this.form.daerah = null
        this.form.negeri = ''
        // Trick to reset/clear native browser form validation state
        this.show = false
        this.$nextTick(() => {
          this.show = true
        })
      },
      createAPI (inputData) {
        console.log('inputData :>> ', inputData);
        this.$axios.post('http://localhost:8080/pengguna', inputData)
          .then(response => {
            this.jawapan = response.data
            console.log(response.data)
          })
          .catch(error => {
            console.log(error)
          })
      }
    }
  }
</script>
```

* Simpan / (Save) fail **daftar.vue**.

* Buka internet browser dan akses ke pautan http://localhost:3000. Klik pautan **Daftar** untuk paparan seperti berikut.

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/a31be0ec47a2d0b730f4aee799cc5373/image.png">

* Masukkan data di paparan dan klik **Hantar**.

* Klik pautan **Pengguna** dan pastikan data yang dimasukkan tersenarai.

### Langkah 3.1.6: Ubah dan Padam data dari MySQL API
Langkah ini adalah untuk menggunakan **Axios** untuk mengubah dan memadam data dari **MySQL API endpoints**

* Sebelum mulakan langkah ini, pastikan aplikasi **MySQL API** untuk **Latihan 7 sudah dimulakan**

* Buka fail **pengguna.vue** di bawah direktori **pages**.

* Buang kod sediada, salin dan tampal kod berikut

```
<template>
    <b-container fluid>
        <br>
        <b-row>
            <b-col>
                <h2>Senarai Pengguna</h2>
            </b-col>
        </b-row>
        <b-row>
            <b-col>
                <b-table striped hover :items="penggunalist" :fields="field">

                    <template #cell(Actions)="row">
                        <b-button @click="showUpdate(row.item)" variant="primary" size="sm">
                            <b-icon icon="pencil-square" aria-label="Help"></b-icon>
                        </b-button>
                        <b-button @click="showDelete(row.item)" variant="danger" size="sm">
                            <b-icon icon="trash" aria-label="Help"></b-icon>
                        </b-button>
                    </template>
                </b-table>
                <template>
                    <b-modal
                        id="update-modal"
                        ref="updateShow"
                        title="Kemaskini Data"
                        hide-footer
                    >
                    <b-form @submit="onSubmit" @reset="onReset">

                        <b-form-group id="input-group-1" label="Nama:" label-for="input-1">
                            <b-form-input
                            id="input-1"
                            v-model="form.nama"
                            disabled
                            ></b-form-input>
                        </b-form-group>

                        <b-form-group id="input-group-2" label="Emel :" label-for="input-2">
                            <b-form-input
                            id="input-2"
                            v-model="form.email"
                            type="email"
                            placeholder="Masukkan emel anda"
                            required
                            ></b-form-input>
                        </b-form-group>

                        <b-form-group id="input-group-3" label="Alamat:" label-for="input-3">
                            <b-form-input
                            id="input-3"
                            v-model="form.alamat"
                            placeholder="Masukkan alamat anda"
                            required
                            ></b-form-input>
                        </b-form-group>

                        <b-form-group id="input-group-4" label="Daerah:" label-for="input-4">
                            <b-form-input
                            id="input-4"
                            v-model="form.daerah"
                            placeholder="Masukkan daerah anda"
                            required
                            ></b-form-input>
                        </b-form-group>

                        <b-form-group id="input-group-5" label="Negeri:" label-for="input-5">
                            <b-form-input
                            id="input-5"
                            v-model="form.negeri"
                            placeholder="Masukkan negeri anda"
                            required
                            ></b-form-input>
                        </b-form-group>
                        <hr />
                        <b-button type="submit" variant="primary">Hantar</b-button>&nbsp;
                        <b-button type="reset" variant="danger">Reset</b-button>
                        <b-button type="button" @click="tutup()">Tutup</b-button>
                        </b-form>
                    </b-modal>
                </template>
            </b-col>
        </b-row>
    </b-container>
</template>

<script>
    export default {
        head: {
            title: "Pengguna"
        },
        data() {
            return {
                penggunalist: [],
                field: ['id', 'nama', 'email', 'alamat','daerah','negeri','Actions'],
                deleteFlag: '',
                selectedID: 0,
                updateShow: false,
                form: {
                    nama: '',
                    email: '',
                    alamat: '',
                    daerah: '',
                    negeri: ''
                },
            }
        },
        mounted () {
            this.getPengguna();
        },
        methods: {
            getPengguna() {
                this.$axios.get('http://localhost:8080/pengguna')
                .then(response => {
                    this.penggunalist = response.data
                    console.log(this.penggunalist)
                })
                .catch(error => {
                    console.log(error)
                })
            },
            onSubmit(event) {
                event.preventDefault()
                this.updatePengguna()
                this.$refs['updateShow'].hide()
            },
            onReset(event) {
                event.preventDefault()
                // Reset our form values
                this.form.email = ''
                this.form.alamat = ''
                this.form.daerah = ''
                this.form.negeri = ''
            },
            tutup(){
                this.$refs['updateShow'].hide()
            },
            showUpdate(selecteditem){
                this.selectedID = selecteditem.id
                console.log('this.selectedID :>> ', this.selectedID);
                this.form.nama = selecteditem.nama
                this.form.email = selecteditem.email
                this.form.alamat = selecteditem.alamat
                this.form.daerah = selecteditem.daerah
                this.form.negeri = selecteditem.negeri
                this.$refs['updateShow'].show()

            },
            showDelete(selecteditem) {
                console.log('selecteditem :>> ', selecteditem);
                this.deleteFlag = ''
                this.$bvModal.msgBoxConfirm('Adakah anda pasti untuk padam data ini?')
                .then(value => {
                    this.deleteFlag = value
                    console.log(this.deleteFlag);
                    if (this.deleteFlag) {
                        this.deletePengguna(selecteditem.id)
                    }
                })
                .catch(err => {
                    // An error occurred
                })
            },
            updatePengguna(){
                let itemData = {
                    email: this.form.email,
                    alamat: this.form.alamat,
                    daerah: this.form.daerah,
                    negeri: this.form.negeri
                }
                console.log(itemData);
                this.$axios.put('http://localhost:8080/pengguna/' + this.selectedID, itemData)
                .then(response => {
                    const jawapan = response.data
                    console.log(jawapan)
                    this.$axios.get('http://localhost:8080/pengguna')
                    .then(response => {
                        this.penggunalist = response.data
                        console.log(this.penggunalist)
                    })
                    .catch(error => {
                        console.log(error)
                    })
                })
                .catch(error => {
                    console.log(error)
                })
            },
            deletePengguna(itemID) {
                this.$axios.delete('http://localhost:8080/pengguna/' + itemID)
                .then(response => {
                    const jawapan = response.data
                    console.log(jawapan)
                    this.$axios.get('http://localhost:8080/pengguna')
                    .then(response => {
                        this.penggunalist = response.data
                        console.log(this.penggunalist)
                    })
                    .catch(error => {
                        console.log(error)
                    })
                })
                .catch(error => {
                    console.log(error)
                })
            },
        },
    }

</script>

<style lang="scss" scoped>

</style>
```

* Simpan / (Save) fail **pengguna.vue**.

* Buka internet browser dan akses ke pautan http://localhost:3000. Klik pautan **Pengguna** seperti paparan berikut. Nota: Data yang disenarai mungkin berbeza.

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/aaa0f11ad32b1d4297b052e16abf04c9/image.png">

* Untuk **UBAH** data, dari senarai data, klik salah satu ikon **Edit** di kolum **Actions**, seperti paparan berikut

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/b8ea737a6929dbc7f7f6e3e77629363e/image.png">

* Tukar data seperti di paparan berikut dan klik **OK** untuk sahkan

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/f3855992de93b5455827b48c4846c9b4/image.png">

* Pastikan data berjaya diubah di senarai

* Untuk **PADAM** data, dari senarai data, klik salah satu ikon **Trash** di kolum **Actions**

* Sila klik **OK** untuk sahkan padam data. Setelah berjaya, pastikan data sudah tiada dari senarai.
