[[_TOC_]]

# Latihan 10: Vuejs
Latihan ini adalah untuk menunjukkan pemahaman membangunkan aplikasi web atau website menggunakan **Vuejs Framework**. 

# Langkah 1.0: Penyediaan Persekitaran Projek
*  Di aplikasi ***Command Prompt*** untuk Windows atau ***Terminal*** untuk MacOS
*  Wujudkan direktori baru - latihan-10

```
> mkdir latihan-10
> cd latihan-10
```

* Taip kod berikut untuk membuka aplikasi Visual Studio Code

```
> code .
```

# Langkah 2.0: _Install Visual Studio Code Extension_
Langkah ini adalah untuk _**install Vue VSCode Snippets extension**_ untuk memudahkan pembangunan aplikasi Vue

* Di Visual Studio Code, dari senarai ikon di kiri, klik _**Extension**_

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/25165bc1d1cd649f14bd96da890e1a44/image.png">

* Buat carian, _**Vue VSCode Snippets**_

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/819d6d3b94dc1c7090658f1a4cab1d85/image.png" width=250>

* Klik Install

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/e3ef6c78ff2839f8b8cf4b6cf76ee113/image.png" width=700>

# Langkah 3.0: Vue.js

## Langkah 3.1: _**Install**_ Vue CLI
Langkah ini adalah untuk _install Vue CLI (Command Line Interface)_ sebagai langkah pertama untuk pembangunan aplikasi menggunakan Vuejs

*  Di ***Command Prompt*** taip kod seperti berikut

```
> npm install -g @vue/cli
```

* Setelah siap, taip kod berikut untuk melihat versi aplikasi Vue CLI:

```
> vue --version
```

## Langkah 3.2: Vue UI
Langkah ini akan menggunakan Vue UI untuk mengurus aplikasi Vue.js

*  Di ***Command Prompt*** taip kod seperti berikut

```
> vue ui
```

* Secara automatik internet browser akan di buka ke pautan http://localhost:8000/dashboard, seperti paparan berikut

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/a855dbe034c222241329b37dee9c5500/image.png">

## Langkah 3.3: Wujudkan aplikasi Vue.js
Langkah ini adalah untuk wujudkan aplikasi Vue.js menggunakan Vue CLI

*  Di ***Command Prompt*** taip kod seperti berikut

```
> vue create vueapp
```
* Untuk soalan berikut, sila pilih **Default ([Vue 2] babel, eslint)** seperti paparan berikut

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/91f6143737118d20e99cd36080d995e9/image.png">

* Setelah selesai, taip kod berikut untuk akses ke direktori projek **vueapp**

```
> cd vueapp
```

* Taip kod berikut untuk membuka aplikasi Visual Studio Code

```
> code .
```

* Buka terminal di Visual Studio Code, dari Menu klik **Terminal -> New Terminal**, dan taip kod seperti berikut untuk mulakan aplikasi **vueapp**

```
> npm run serve
```

* Setelah berjaya, buka internet browser dan akses ke pautan http://localhost:8080/ seperti paparan berikut

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/2bae2c627c9c310c2ca24851f8c59cb2/image.png">


## Langkah 3.4: Ubah aplikasi Vue.js
Langkah ini adalah untuk memahami Vue.js components

*  Buka fail **App.vue** di direktori **src**, padamkan semua kod, salin dan tampal kod berikut

```
<template>
  <div id="app">
    <h1> {{ message }} </h1>
  </div>
</template>

<script>

export default {
  name: 'My Vue Page',
  data () {
      return {
          message: 'Salam Muafakat!!'
      }
    },
}
</script>

<style>
#app {
  font-family: Avenir, Helvetica, Arial, sans-serif;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  text-align: center;
  color: #2c3e50;
  margin-top: 60px;
}
</style>
```

* Simpan / **(Save)** fail. Sila rujuk paparan berikut

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/b1ececa737c3d5e5e9273e2a21cc821d/image.png" width=600>

* Buka kembali internet browser dan akses ke pautan http://localhost:8080/ seperti paparan berikut

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/7a0ead4e3cf9d41cafbbf4c0f4e03707/image.png">

* Di terminal, untuk hentikan applikasi Vue.js, tekan kekunci **Ctrl-C**

## Langkah 3.5: Penggunaan BootstrapVue UI Framework
Langkah ini adalah untuk menggunakan BootstrapVue sebagai UI Framework

* Di terminal Visual Studio Code, taip kod seperti berikut

```
> npm install vue bootstrap bootstrap-vue
```

* Buka fail **main.js** di direktori **src**, padamkan semua kod, salin dan tampal kod berikut 

```
import Vue from 'vue'
import App from './App.vue'

import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

// Import Bootstrap an BootstrapVue CSS files (order is important)
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

// Make BootstrapVue available throughout your project
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')

```

* Simpan / **(Save)** fail **main.js**.

*  Buka fail **App.vue** di direktori **src**, padamkan semua kod, salin dan tampal kod berikut

```
<template>
  <div id="app">
    <b-container>
      <h1> {{ message }} </h1>
      <b-card
        title="Card Title"
        img-src="https://picsum.photos/600/300/?image=25"
        img-alt="Image"
        img-top
        tag="article"
        style="max-width: 20rem;"
        class="mb-2"
      >
        <b-card-text>
          Some quick example text to build on the card title and make up the bulk of the card's content.
        </b-card-text>

        <b-button href="#" variant="primary">Go somewhere</b-button>
      </b-card>
    </b-container>
  </div>
</template>

<script>

export default {
  name: 'My Vue Page',
  data () {
      return {
          message: 'Salam Muafakat!!'
      }
    },
}
</script>

<style>
#app {
  font-family: Avenir, Helvetica, Arial, sans-serif;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  text-align: center;
  color: #2c3e50;
  margin-top: 60px;
}
</style>
```

* Simpan / **(Save)** fail **App.vue**.
* Di terminal Visual Studio Code, taip kod seperti berikut untuk mulakan aplikasi **vueapp**

```
> npm run serve
```

* Setelah berjaya, buka internet browser dan akses ke pautan http://localhost:8080/ seperti paparan berikut

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/e98bdfa20f19962cceb89387d56f56b1/image.png">

* Di terminal, untuk hentikan applikasi Vue.js, tekan kekunci **Ctrl-C**

## Langkah 3.6: Penggunaan Vue Router
Langkah ini adalah untuk memahami penggunaan Vue Router sebagai **navigation** ke pautan laman web aplikasi 

### Langkah 3.6.1: _**Install**_ Vue Router
* Di terminal Visual Studio Code, taip kod seperti berikut

```
> vue add router
```

* Untuk soalan berikut, sila taip  **y**  seperti paparan berikut

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/6e5bc54ea4882a95493fcc7cc91c136e/image.png">

* Sila sahkan beberapa direktori baru seperti **router** dan **views** wujud seperti paparan berikut

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/e3d524d8f36a5b4666a4e34a436cb566/image.png">

* Di terminal Visual Studio Code, taip kod seperti berikut untuk mulakan aplikasi **vueapp**

```
> npm run serve
```

* Setelah berjaya, buka internet browser dan akses ke pautan http://localhost:8080/. Pastikan ada 2 pautan **Home** dan **About** wujud seperti paparan berikut

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/f429de7821d353cb666726d55f97fe6d/image.png">

* Buka fail **/views/About.vue**, padamkan semua kod, salin dan tampal kod berikut

```
<template>
  <div id="app">
    <b-container>
      <h1> {{ message }} </h1>
      <b-card
        title="Card Title"
        img-src="https://picsum.photos/600/300/?image=25"
        img-alt="Image"
        img-top
        tag="article"
        style="max-width: 20rem;"
        class="mb-2"
      >
        <b-card-text>
          Some quick example text to build on the card title and make up the bulk of the card's content.
        </b-card-text>

        <b-button href="#" variant="primary">Go somewhere</b-button>
      </b-card>
    </b-container>
  </div>
</template>

<script>

export default {
  name: 'My Vue Page',
  data () {
      return {
          message: 'Salam Muafakat!!'
      }
    },
}
</script>

<style>
#app {
  font-family: Avenir, Helvetica, Arial, sans-serif;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  text-align: center;
  color: #2c3e50;
  margin-top: 60px;
}
</style>
```

* Simpan / **(Save)** fail **About.vue**. Buka internet browser dan akses ke pautan http://localhost:8080/. Klik pautan **About** untuk paparan seperti berikut

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/2b2f27ee2d300cc1631f4f0c67c044cc/image.png">

### Langkah 3.6.2: Wujudkan fail baru - Daftar.vue

* Wujudkan fail **Daftar.vue** di bawah direktori **views**. **Right-click** di direktori **views**, klik **New File** seperti di paparan berikut

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/35393e5ae694a685b39c6c65321d51fd/image.png">

* Salin dan tampal kod berikut

```
<template>
  <div>
    <b-container>
      <h1> Pendaftaran </h1>
    </b-container>
  </div>
</template>

<script>

</script>

<style>

</style>
```

* Simpan / **(Save)** fail **Daftar.vue**. 

### Langkah 3.6.3: Ubah fail /router/index.js dan App.vue

* Buka fail **index.js** dari direktori **router**. Padam kod sediada, salin dan tampal kod berikut

```
import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import About from '../views/About.vue'
import Daftar from '../views/Daftar.vue'


Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    component: About
  },
  {
    path: '/daftar',
    name: 'Daftar',
    component: Daftar
  }

]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
```
* Simpan / **(Save)** fail **/router/index.js**. 

* Buka fail **App.vue**. Padam kod sediada, salin dan tampal kod berikut

```
<template>
  <div id="app">
    <div id="nav">
      <router-link to="/">Home</router-link> |
      <router-link to="/about">About</router-link> |
      <router-link to="/daftar">Daftar</router-link>
    </div>
    <router-view/>
  </div>
</template>

<style>
#app {
  font-family: Avenir, Helvetica, Arial, sans-serif;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  text-align: center;
  color: #2c3e50;
}

#nav {
  padding: 30px;
}

#nav a {
  font-weight: bold;
  color: #2c3e50;
}

#nav a.router-link-exact-active {
  color: #42b983;
}
</style>
```
* Simpan / **(Save)** fail **App.vue**. 

* Buka internet browser dan akses ke pautan http://localhost:8080/. Klik pautan **Daftar** untuk paparan seperti berikut

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/08843c2fa02bd28b7f617aa2c6c8c95a/image.png">

## Langkah 3.7: Penggunaan Vue Components
Langkah ini adalah untuk memahami penggunaan **Vue Components** dengan membuat _**component**_ **Pengumuman**

### Langkah 3.7.1: Wujudkan fail untuk _Components_

* Wujudkan fail **Pengumuman.vue** di bawah direktori **components**. **Right-click** di direktori **components**, klik **New File** seperti di paparan berikut

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/edb2ff5dc25f7170fa7d949800f8fce1/image.png">

* Salin dan tampal kod berikut

```
<template>
  <div>
    <b-container>
        <b-card
        title="Card Title"
        img-src="https://picsum.photos/600/300/?image=25"
        img-alt="Image"
        img-top
        tag="article"
        style="max-width: 20rem;"
        class="mb-2"
      >
        <b-card-text>
          Some quick example text to build on the card title and make up the bulk of the card's content.
        </b-card-text>

        <b-button href="#" variant="primary">Go somewhere</b-button>
      </b-card>
    </b-container>
  </div>
</template>

<script>
export default {
    name: 'Pengumuman',
}
</script>

<style>

</style>
```

* Simpan / **(Save)** fail **Pengumuman.vue**. 

* Buka fail **Home.vue**, padam kod sediada, salin dan tampal kod berikut

```
<template>
  <div class="home">
    <img alt="Vue logo" src="../assets/logo.png">
    <HelloWorld msg="Welcome to Your Vue.js App"/>
    <Pengumuman />
  </div>
</template>

<script>
// @ is an alias to /src
import HelloWorld from '@/components/HelloWorld.vue';
import Pengumuman from "@/components/Pengumuman.vue";

export default {
  name: 'Home',
  components: {
    HelloWorld,
    Pengumuman
  }
}
</script>
```

* Simpan / **(Save)** fail **Home.vue**. 

* Buka internet browser dan akses ke pautan http://localhost:8080/. Klik pautan **Daftar** untuk paparan seperti berikut

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/f8d6c61a71dc7abce1ba6dcce8184bdf/image.png">

### Langkah 3.7.2: Menghantar data ke _Components_ menggunakan _Props_

* Buka fail **Pengumuman.vue**, padam kod sediada, salin dan tampal kod berikut

```
<template>
  <div>
    <b-container>
        <b-card
        :title="tajuk"
        :img-src="photo"
        img-alt="Image"
        img-top
        tag="article"
        style="max-width: 20rem;"
        class="mb-2"
      >
        <b-card-text>
          {{ perincian }}
        </b-card-text>

        <b-button href="#" variant="primary">{{ button }}</b-button>
      </b-card>
    </b-container>
  </div>
</template>

<script>
export default {
    name: 'Pengumuman',
    props: {
        tajuk: String,
        perincian: String,
        photo: String,
        button: String
    }
}
</script>

<style>

</style>
```
* Simpan / **(Save)** fail **Pengumuman.vue**. 

* Buka fail **Home.vue**, padam kod sediada, salin dan tampal kod berikut

```
<template>
  <div class="home">
    <img alt="Vue logo" src="../assets/logo.png">
    <HelloWorld msg="Welcome to Your Vue.js App"/>
    <Pengumuman :tajuk=tajuk :perincian=perincian :photo=photo button="Lihat Semua" />
  </div>
</template>

<script>
// @ is an alias to /src
import HelloWorld from '@/components/HelloWorld.vue';
import Pengumuman from "@/components/Pengumuman.vue";

export default {
  name: 'Home',
  components: {
    HelloWorld,
    Pengumuman
  },
  data () {
    return {
      tajuk: 'Latihan Aplikasi Moden',
      perincian: 'Latihan untuk memahami penggunaan teknologi moden untuk membangunkan aplikasi web',
      photo: 'https://picsum.photos/600/300/?image=25'
    }
  }
}
</script>
```
* Simpan / **(Save)** fail **Home.vue**. 

## Langkah 3.8: Penggunaan _Vue Forms Input Binding_
Langkah ini adalah untuk memahami penggunaan **Vue Forms Input Binding** 

* Buka fail **Daftar.vue**, padam kod sediada, salin dan tampal kod berikut

```
<template>
  <div>
    <b-container>
      <b-row class="justify-content-md-center">
        <b-col cols="8" md="auto">
        <h1> Pendaftaran </h1>
        <b-form @submit="onSubmit" @reset="onReset" v-if="show">

        <b-form-group 
            id="input-group-1" 
            label="Nama:" 
            label-for="input-1"
        >
            <b-form-input
            id="input-1"
            v-model="form.name"
            placeholder="Masukkan nama anda"
            required
            ></b-form-input>
        </b-form-group>

        <b-form-group
            id="input-group-2"
            label="Emel :"
            label-for="input-2"
            description="Emel mestilah emel yang sah"
        >
            <b-form-input
            id="input-2"
            v-model="form.email"
            type="email"
            placeholder="Masukkan emel anda"
            required
            ></b-form-input>
        </b-form-group>

        <b-form-group id="input-group-3" label="Daerah:" label-for="input-3">
            <b-form-select
            id="input-3"
            v-model="form.daerah"
            :options="daerah"
            required
            ></b-form-select>
        </b-form-group>
        <hr />
        <b-button type="submit" variant="primary">Hantar</b-button>&nbsp;
        <b-button type="reset" variant="danger">Reset</b-button>
        </b-form>
        </b-col>
      </b-row>
    </b-container>
  </div>
</template>

<script>
  export default {
    data() {
      return {
        form: {
          email: '',
          name: '',
          daerah: null,
        },
        daerah: [{ text: 'Pilih daerah', value: null }, 'Johor Bahru', 'Kota Tinggi', 'Segamat'],
        show: true
      }
    },
    methods: {
      onSubmit(event) {
        event.preventDefault()
        alert(JSON.stringify(this.form))
      },
      onReset(event) {
        event.preventDefault()
        // Reset our form values
        this.form.email = ''
        this.form.name = ''
        this.form.daerah = null
        // Trick to reset/clear native browser form validation state
        this.show = false
        this.$nextTick(() => {
          this.show = true
        })
      }
    }
  }
</script>
```
* Simpan / **(Save)** fail **Daftar.vue**. 

* Buka internet browser dan akses ke pautan http://localhost:8080/. Klik pautan **Daftar** untuk paparan seperti berikut

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/93ba73c2e5075c325ff96ddcd52cb778/image.png">

* Masukkan maklumat dan klik **Hantar** untuk uji **Vue Forms**
