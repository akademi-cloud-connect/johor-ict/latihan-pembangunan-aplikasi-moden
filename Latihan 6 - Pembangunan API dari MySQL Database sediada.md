[[_TOC_]]

# Latihan 6: Pembangunan API dari MySQL Database sediada
Latihan ini adalah untuk menunjukkan pemahaman membangunkan MySQL API dari MySQL database yang sudah mempunyai data dengan menggunakan **_PRISMA ORM and ExpressJS Framework_**. Untuk latihan ini MySQL database server akan disediakan di persekitaran pengkomputeran awan.

**Nota:**
Jika hendak menyediakan persekitaran MySQL database untuk di komputer anda, sila rukjuk pautan berikut untuk maklumat lebih lanjut: https://dev.mysql.com/doc/mysql-installation-excerpt/8.0/en/windows-installation.html

# Langkah 1.0: Penyediaan Persekitaran Projek
*  Di aplikasi ***Command Prompt*** untuk Windows atau ***Terminal*** untuk MacOS
*  Wujudkan direktori baru - latihan-6

```
> mkdir latihan-6
> cd latihan-6
```
* Taip kod berikut untuk membuka aplikasi Visual Studio Code

```
> code .
```

# Langkah 2.0: _Install Visual Studio Code Prisma Extension_
Langkah ini adalah untuk _install Prisma extensions_ untuk memudahkan pembangunan MySQL API

* Di Visual Studio Code, dari senarai ikon di kiri, klik _**Extension**_

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/25165bc1d1cd649f14bd96da890e1a44/image.png">

* Buat carian, _**Prisma**_

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/5f4419f955484a1ea331170d4a44eebb/image.png" width=250>

* Klik Install

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/985a2a6ba548f3b9fefdd7bbe585b274/image.png" width=600>

# Langkah 3.0: _**Initialize**_ Persekitaran NodeJS
Langkah ini adalah untuk _initialize NodeJS environment_ sebagai langkah pertama untuk pembangunan API

*  Buka terminal di Visual Studio Code, dari Menu klik _**Terminal -> New Terminal**_ 
*  Di terminal taip kod seperti berikut

```
> npm init -y
```
* Pastikan fail **package.json** wujud.

# Langkah 4.0: _Install & Initialize_ Persekitaran Prisma
Langkah ini adalah untuk _install and initialize Prisma environment_ sebagai langkah untuk pembangunan MSQL API

* Di terminal di Visual Studio Code, taip kod seperti berikut untuk _**install Prisma Client**_

```
> npm install prisma --save-dev
```

* Setelah selesai, di fail **package.json**, pastikan **prisma** adalah salah satu **devDependencies**. Sila rujuk paparan berikut:

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/b492836204c241a3c70d1c2e1f0c75f2/image.png" width=600>

* Di terminal di Visual Studio Code, taip kod seperti berikut untuk _**initialize Prisma environment**_

```
> npx prisma init
```

* Jika berjaya, sila rujuk paparan berikut:

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/b230f84d6761e834ca280c292a03c54d/image.png" width=600>

* Pastikan direktori **prisma**, fail **.env** dan **prisma/schema.prisma** diwujudkan. Sila rujuk paparan berikut:

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/04b74e7844d57c9602d0299979353f3b/image.png">

# Langkah 5.0: _Connecting to MySQL Database using Prisma_
Langkah ini adalah untuk membuat _connection_ ke MSQL database

* Buka fail **.env**, tukar maklumat **DATABASE_URL** dengan salin dan tampal kod seperti berikut

```
DATABASE_URL="mysql://pentadbir:rahsia@167.71.217.175:3306/testdb"
```

```
Nota:
- root : adalah username untuk akses ke MySQL database
- rahsia : adalah katalaulan untuk username root
- 167.71.217.175 : adalah database server URL atau IP address localhost / 127.0.0.1 jika dari komputer sendiri
- 3306 : adalah port untuk MySQL server
- testdb : adalah nama database
```

* Simpan / (Save) fail.

* Buka fail **schema.prisma** di direktori **prisma**, tukar maklumat **datasource db** dengan salin dan tampal kod seperti berikut

```
datasource db {
  provider = "mysql"
  url      = env("DATABASE_URL")
}
```

* Simpan / (Save) fail. Sila rujuk paparan berikut:

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/422b883caed812c562d867cf04a5808d/image.png" width=600>

# Langkah 6.0: Penggunaan _Prisma Introspect_
Langkah ini adalah untuk menggunakan **Prisma Introspect** untuk akses semua maklumat **table** di **testdb** database dan menjana **schema** untuk **Prisma** 

* Di terminal di Visual Studio Code, taip kod seperti berikut

```
> npx prisma introspect
```

* Setelah selesai, buka fail **schema.prisma**, Sila rujuk paparan berikut:

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/8d12dfa83db0593d5ffbdec115b552a0/image.png" width=600>

# Langkah 7.0: Penggunaan _Prisma Studio_
Langkah ini adalah untuk menggunakan **Prisma Studio** untuk akses MySQL database

* Buka aplikasi Command Prompt untuk Windows atau Terminal untuk MacOS dan pastikan berada di direktori projek **latihan-6**, taip kod seperti berikut

```
> npx prisma studio
```

* Jika berjaya, sila rujuk paparan berikut:

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/5f95d8108bedb939ed67eb348a773b77/image.png" width=600>

* Secara automatik juga, **internet browser** dengan URL http://localhost:5555/ akan dibuka seperti paparan berikut

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/7da9841ad5d0d1ee6485ed03b314bbad/image.png" width=500>

* Untuk melihat data di database. Klik **pengguna** di senarai **All Models**, sila rujuk paparan berikut

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/93ec26f059e6dd367d554e191af8a89b/image.png">

# Langkah 8.0: _Install ExpressJS Framework, body-parser & Prisma Client_
Langkah ini adalah untuk _Install ExpressJS Framework & Prisma Client_
<br>

*  Di terminal taip kod seperti berikut

```
> npm install express @prisma/client body-parser
```

* Setelah selesai, di fail _**package.json**_, pastikan _**express, body-parser, @prisma/client**_ adalah salah satu _**dependencies**_. Sila rujuk paparan berikut:

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/51b1094c486588b8ac08be6560d95eaa/image.png" width=500>

# Langkah 9.0: Penggunaan _ExpressJS Framework_
Langkah ini adalah untuk membangunkan API menggunakan ExpressJS Framework
* Di Visual Studio Code, wujudkan fail **index.js**. Dari Menu, klik **_File -> New File_**
* Salin dan tampal kod berikut

```
const express = require('express');
const app = express();

app.get('/', (req, res) => {
    res.send('Salam Muafakat!');
});

app.listen(8080, () => {
    console.log('App listening on port 8080!');
});

//Run app, then load http://localhost:8080 in a browser to see the output.
```
* Simpan / (_**Save**_) fail. Sila rujuk paparan berikut:

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/b55ab9fe0fc002edbaceb2d2703e708e/image.png" width=600>

# Langkah 10.0: Pengunaan NPM RUN
Langkah ini adalah untuk membuat konfigurasi NodeJS dengan menggunakan _command **NPM RUN**_
* Buka fail **package.json**.
* Tukar kod _**"script"**_ dengan salin dan tampal kod seperti berikut

```
"scripts": {
    "start": "node index.js"
  },
```
* Simpan / (_**Save**_) fail. Sila rujuk paparan berikut:

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/aca4f07d1affdece939e96ba4ec28320/image.png" width=300>

# Langkah 11.0: Uji Penggunaan ExpressJS Framework

* Di terminal taip kod seperti berikut:

```
> npm run start
```
* Jika berjaya, berikut adalah paparan maklumat di terminal (maklumat akan berbeza di terminal anda):
```
> latihan-6@1.0.0 start /Users/hanafiah/Documents/Development/node/latihan/latihan-6
> node index.js

App listening on port 8080!
```
* Buka internet browser, dan layari **http://localhost:8080** dengan paparan berikut

```
Salam Muafakat!
```

* Di terminal, untuk **_stop_** applikasi NodeJS, tekan kekunci **_Ctrl-C_**


# Langkah 12.0: _READ API for MySQL_
Langkah ini adalah untuk membangunkan _Read API for MySQL_ untuk membaca data di **table Pengguna**

* Tukar kod di fail **index.js** dengan salin dan tampal kod berikut 

```
const express = require('express');
const app = express();

const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();

app.get('/', (req, res) => {
    res.send('Salam Muafakat!');
});

// READ API
app.get('/pengguna', async (req, res) => {
    const jawapan = await prisma.pengguna.findMany();
    res.json(jawapan);
  });

app.listen(8080, () => {
    console.log('App listening on port 8080!');
});

//Run app, then load http://localhost:8080 in a browser to see the output.
```

* Simpan / (_**Save**_) fail. Sila rujuk paparan berikut:

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/e18b7f4abef8863a1d2e381f9b54851c/image.png" width=600>


# Langkah 13.0: Uji _READ API for MySQL_

* Di terminal taip kod seperti berikut:

```
> npm run start
```

* Wujudkan fail **uji.http**. Dari Menu, klik **_File -> New File_**
* Salin dan tampal kod berikut 

```
http://localhost:8080

###
http://localhost:8080/pengguna
```
* Simpan / (_**Save**_) fail. Sila rujuk paparan berikut:

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/e806d62f532ebffc72f8c2c75555fe29/image.png">

* Di fail uji.http, klik **Send Request**. Sila rujuk paparan berikut:

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/5cb73afd0f131a6f22a2a10bad31a98f/image.png">

* Berikut adalah paparan di sebelah kanan jika berjaya

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/3e09af332c8cec5decdadf76dc118dd7/image.png" width=500>

* Sila pastikan data adalah sama dengan maklumat laman Prisma Studio seperti di Langkah 7.0
* Di terminal, untuk **_stop_** applikasi NodeJS, tekan kekunci **_Ctrl-C_**

# Langkah 14.0: _CREATE / Add API for MySQL_
Langkah ini adalah untuk membangunkan _Create / Add API for MySQL_ untuk wujudkan atau tambah data di **table Pengguna**

* Tukar kod di fail **index.js** dengan salin dan tampal kod berikut 

```
const express = require('express');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();

app.get('/', (req, res) => {
    res.send('Salam Muafakat!');
});

// READ API
app.get('/pengguna', async (req, res) => {
    const jawapan = await prisma.pengguna.findMany();
    res.json(jawapan);
});

// CREATE / ADD API
// contoh maklumat yang dihantar
// {
//     "nama": "user1",
//     "email": "user@cloud-connect.asia",
//     "alamat": "Kg Melayu Kangkar Pulai",
//     "daerah": "Johor Bahru",
//     "negeri": "Johor"
// }
app.post('/pengguna', async (req, res) => {
    const jawapan = await prisma.pengguna.create({ data: req.body });
    res.json(jawapan);
});

app.listen(8080, () => {
    console.log('App listening on port 8080!');
});

//Run app, then load http://localhost:8080 in a browser to see the output.
```

* Simpan / (_**Save**_) fail. Sila rujuk paparan berikut:

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/ffea3ce029f17dde16999349c075b9f4/image.png" width=600>


# Langkah 15.0: Uji _CREATE / Add API for MySQL_

* Di terminal taip kod seperti berikut:

```
> npm run start
```

* Di fail **uji.http**. Salin dan tampal kod berikut di akhir fail

```
###
POST http://localhost:8080/pengguna
content-type: application/json

{
    "nama": "user1",
    "email": "user@cloud-connect.asia",
    "alamat": "Kg Melayu Kangkar Pulai",
    "daerah": "Johor Bahru",
    "negeri": "Johor"
}
```
* Simpan / (_**Save**_) fail. Sila rujuk paparan berikut:

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/0d44499ee5dfe789b451bd4368b42f8c/image.png" width=400>

* Di fail uji.http, klik **Send Request**. Sila rujuk paparan berikut:

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/c3fffdf211f44a1267ef36ab48790f4b/image.png">

* Sila pastikan data yang dimasukkan tersenarai dalam database dengan membuat ujian seperti Langkah 13.0 

* Di terminal, untuk **_stop_** applikasi NodeJS, tekan kekunci **_Ctrl-C_**

# Langkah 16.0: _UPDATE API for MySQL_
Langkah ini adalah untuk membangunkan _Update API for MySQL_ untuk menukar data di **table Pengguna**

* Tukar kod di fail **index.js** dengan salin dan tampal kod berikut 

```
const express = require('express');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();

app.get('/', (req, res) => {
    res.send('Salam Muafakat!');
});

// READ API
app.get('/pengguna', async (req, res) => {
    const jawapan = await prisma.pengguna.findMany();
    res.json(jawapan);
});

// CREATE / ADD API
// contoh maklumat yang dihantar
// {
//     "nama": "user1",
//     "email": "user@cloud-connect.asia",
//     "alamat": "Kg Melayu Kangkar Pulai",
//     "daerah": "Johor Bahru",
//     "negeri": "Johor"
// }
app.post('/pengguna', async (req, res) => {
    const jawapan = await prisma.pengguna.create({ data: req.body });
    res.json(jawapan);
});

// Update API
// contoh maklumat yang dihantar
// {
//     "alamat": "Kg Nong Chik",
// }
app.put('/pengguna/:userid', async (req, res) => {
    const userid = req.params.userid
    const jawapan = await prisma.pengguna.update({ 
        where: { id : Number(userid) },
        data: req.body
    });
    res.json(jawapan);
});

app.listen(8080, () => {
    console.log('App listening on port 8080!');
});

//Run app, then load http://localhost:8080 in a browser to see the output.
```

* Simpan / (_**Save**_) fail. Sila rujuk paparan berikut:

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/08c61ff1d0cebec1b1009f99fa00d8ef/image.png" width=600>


# Langkah 17.0: Uji _UPDATE API for MySQL_
Langkah ini adalah untuk menukar data **alamat** pada pengguna **id = 2** iaitu **user1**

* Di terminal taip kod seperti berikut:

```
> npm run start
```

* Di fail **uji.http**. Salin dan tampal kod berikut di akhir fail

```
###
PUT http://localhost:8080/pengguna/2
content-type: application/json

{
    "alamat": "Kg Nong Chik"
}
```
* Simpan / (_**Save**_) fail. Sila rujuk paparan berikut:

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/b1f8cd176661e5df906773579c1a70de/image.png" width=400>

* Di fail uji.http, klik **Send Request**. Sila rujuk paparan berikut:

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/47f4065e9a0ec7e0c8ccacdce60c7b34/image.png">

* Sila pastikan data **alamat** untuk **user1** - **Kg Melayu Kangkar Pulai** di tukar ke **Kg Nong Chik** dengan membuat ujian seperti Langkah 13.0 

* Di terminal, untuk **_stop_** applikasi NodeJS, tekan kekunci **_Ctrl-C_**


# Langkah 18.0: DELETE API for MySQL_
Langkah ini adalah untuk membangunkan _DELETE API for MySQL_ untuk membuang data di **table Pengguna**

* Tukar kod di fail **index.js** dengan salin dan tampal kod berikut 

```
const express = require('express');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();

app.get('/', (req, res) => {
    res.send('Salam Muafakat!');
});

// READ API
app.get('/pengguna', async (req, res) => {
    const jawapan = await prisma.pengguna.findMany();
    res.json(jawapan);
});

// CREATE / ADD API
// contoh maklumat yang dihantar
// {
//     "nama": "user1",
//     "email": "user@cloud-connect.asia",
//     "alamat": "Kg Melayu Kangkar Pulai",
//     "daerah": "Johor Bahru",
//     "negeri": "Johor"
// }
app.post('/pengguna', async (req, res) => {
    const jawapan = await prisma.pengguna.create({ data: req.body });
    res.json(jawapan);
});

// UPDATE API
// contoh maklumat yang dihantar
// {
//     "alamat": "Kg Nong Chik",
// }
app.put('/pengguna/:userid', async (req, res) => {
    const userid = req.params.userid
    const jawapan = await prisma.pengguna.update({ 
        where: { id : Number(userid) },
        data: req.body
    });
    res.json(jawapan);
});

// DELETE API
app.delete('/pengguna/:userid', async (req, res) => {
    const userid = req.params.userid
    const jawapan = await prisma.pengguna.delete({ 
        where: { id : Number(userid) },
    });
    res.json(jawapan);
});

app.listen(8080, () => {
    console.log('App listening on port 8080!');
});

//Run app, then load http://localhost:8080 in a browser to see the output.
```

* Simpan / (_**Save**_) fail. Sila rujuk paparan berikut:

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/05b9324fd8f94a73174eaa15c041ec5d/image.png" width=600>


# Langkah 17.0: Uji _DELETE API for MySQL_
Langkah ini adalah untuk membuang data pengguna **id = 2** iaitu **user1**

* Di terminal taip kod seperti berikut:

```
> npm run start
```

* Di fail **uji.http**. Salin dan tampal kod berikut di akhir fail

```
###
DELETE http://localhost:8080/pengguna/2
```
* Simpan / (_**Save**_) fail. Sila rujuk paparan berikut:

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/1e0f22e5561f3c967af2aa51a613e855/image.png" width=400>

* Di fail uji.http, klik **Send Request**. Sila rujuk paparan berikut:

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/2f13683196f515bc7f235a57eadee29c/image.png">

* Sila pastikan data **user1** sudah dibuang dengan membuat ujian seperti Langkah 13.0 

* Di terminal, untuk **_stop_** applikasi NodeJS, tekan kekunci **_Ctrl-C_**
