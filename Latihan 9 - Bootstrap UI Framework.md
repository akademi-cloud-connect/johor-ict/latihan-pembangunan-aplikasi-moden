[[_TOC_]]

# Latihan 9: Bootstrap UI Framework
Latihan ini adalah untuk menunjukkan pemahaman menggunakan **Bootstrap UI Framework**. 

# Langkah 1.0: Penyediaan Persekitaran Projek
*  Di aplikasi ***Command Prompt*** untuk Windows atau ***Terminal*** untuk MacOS
*  Wujudkan direktori baru - latihan-9

```
> mkdir latihan-9
> cd latihan-9
```
* Taip kod berikut untuk membuka aplikasi Visual Studio Code

```
> code .
```
# Langkah 2.0: _Install Visual Studio Code Extension_
Langkah ini adalah untuk _install Live Server_ untuk memudahkan pembangunan laman web

* Di Visual Studio Code, dari senarai ikon di kiri, klik _**Extension**_

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/25165bc1d1cd649f14bd96da890e1a44/image.png">

* Buat carian, _**live server**_

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/766727700249d96fc0a3e5049c4334f4/image.png" width=250>

* Klik Install

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/d536ab54a9ac3fa35de6596446b21c54/image.png" width=700>


# Langkah 3.0: Wujudkan index.html

* Wujudkan fail **index.html**. Dari Menu, klik **_File -> New File_**

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/4df2812297bbbcc14e7f6d23731a110f/image.png" width=250>

* Salin dan tampal kod berikut dan simpan / (_**Save**_) fail

```
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Latihan 9</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>
</head>
<body>

</body>
</html>
```
# Langkah 4.0: Penggunaan Bootstrap
Latihan ini adalah untuk menunjukkan pemahaman menggunakan **Bootstrap UI Framework**. 

## Langkah 4.1: Containers Layout
_Containers are a fundamental building block of Bootstrap that contain, pad, and align your content within a given device or viewport._

Nota: Sila ke pautan ini untuk maklumat lanjut https://getbootstrap.com/docs/5.1/layout/containers/


* Buka fail **index.html**. Salin dan tampal kod berikut di antara dan simpan / (_**Save**_) fail

```
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Latihan 9</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>
</head>
<body>
    <div class="container">
        Salam Muafakat!
    </div>
    <div class="container-sm">100% wide until small breakpoint</div>
    <div class="container-md">100% wide until medium breakpoint</div>
    <div class="container-lg">100% wide until large breakpoint</div>
    <div class="container-xl">100% wide until extra large breakpoint</div>
    <div class="container-xxl">100% wide until extra extra large breakpoint</div>
</body>
</html>
</body>
</html>
```

* Untuk uji, **right-click** di fail **index.html** dan pilih **Open with Live Server** seperti paparan berikut

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/f1a3a9e7d8235e0d93048bd64b09833f/image.png" width=350>

*  Internet browser akan dibuka secara automatik ke pautan http://127.0.0.1:5500/index.html. Sila rujuk paparan berikut

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/d82d03f3091c524dfd018b4fc9227bb9/image.png" width=450>

* Sahkan ujian dengan lihat perubahan laman web jika membesar dan mengecilkan saiz internet browser

## Langkah 4.2: Grid Layout
_Mobile-first flexbox grid to build layouts of all shapes and sizes thanks to a twelve column system, six default responsive tiers, Sass variables and mixins, and dozens of predefined classes._

Nota: Sila ke pautan ini untuk maklumat lanjut https://getbootstrap.com/docs/5.1/layout/grid/


* Wujudkan fail **grid.html**. Dari Menu, klik **_File -> New File_**. Salin dan tampal kod berikut di antara dan simpan / (_**Save**_) fail

```
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Latihan 9 - Grid</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>
</head>
<body>
    <div class="container">
        <div class="row justify-content-md-center">
          <div class="col col-lg-2">
            1 of 3
          </div>
          <div class="col-md-auto">
            Variable width content
          </div>
          <div class="col col-lg-2">
            3 of 3
          </div>
        </div>
        <div class="row">
          <div class="col">
            1 of 3
          </div>
          <div class="col-md-auto">
            Variable width content
          </div>
          <div class="col col-lg-2">
            3 of 3
          </div>
        </div>
    </div>
</body>
</html>
```

* Untuk uji, **right-click** di fail **grid.html** dan pilih **Open with Live Server**

* Internet browser akan dibuka secara automatik ke pautan http://127.0.0.1:5500/grid.html. Sila rujuk paparan berikut

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/7c4936aaa0dee41bc223e978fe8f13a4/image.png">

* Sahkan ujian dengan lihat perubahan laman web jika membesar dan mengecilkan saiz internet browser

## Langkah 4.3: Cards Components
_Bootstrap’s cards provide a flexible and extensible content container with multiple variants and options._

Nota: Sila ke pautan ini untuk maklumat lanjut https://getbootstrap.com/docs/5.1/components/card/


* Wujudkan fail **card.html**. Dari Menu, klik **_File -> New File_**. Salin dan tampal kod berikut di antara dan simpan / (_**Save**_) fail

```
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Latihan 9 - Cards</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>
</head>
<body>
    <div class="container">
        <div class="row row-cols-1 row-cols-md-3 g-4">
            <div class="col">
            <div class="card">
                <img src="http://www.cloud-connect.asia/wp-content/uploads/2016/01/cloudconnect-logo.png" class="card-img-top" alt="cloudconnect-logo">
                <div class="card-body">
                <h5 class="card-title">Card title</h5>
                <p class="card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                </div>
            </div>
            </div>
            <div class="col">
            <div class="card">
                <img src="http://www.cloud-connect.asia/wp-content/uploads/2016/01/cloudconnect-logo.png" class="card-img-top" alt="cloudconnect-logo">
                <div class="card-body">
                <h5 class="card-title">Card title</h5>
                <p class="card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                </div>
            </div>
            </div>
            <div class="col">
            <div class="card">
                <img src="http://www.cloud-connect.asia/wp-content/uploads/2016/01/cloudconnect-logo.png" class="card-img-top" alt="cloudconnect-logo">
                <div class="card-body">
                <h5 class="card-title">Card title</h5>
                <p class="card-text">This is a longer card with supporting text below as a natural lead-in to additional content.</p>
                </div>
            </div>
            </div>
            <div class="col">
            <div class="card">
                <img src="http://www.cloud-connect.asia/wp-content/uploads/2016/01/cloudconnect-logo.png" class="card-img-top" alt="cloudconnect-logo">
                <div class="card-body">
                <h5 class="card-title">Card title</h5>
                <p class="card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                </div>
            </div>
            </div>
        </div>
    </div>
</body>
</html>
```

* Untuk uji, **right-click** di fail **card.html** dan pilih **Open with Live Server**

* Internet browser akan dibuka secara automatik ke pautan http://127.0.0.1:5500/card.html. Sila rujuk paparan berikut

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/7254b633c77348c29cf1d3b041d71b0f/image.png">

* Sahkan ujian dengan lihat perubahan laman web jika membesar dan mengecilkan saiz internet browser

## Langkah 4.4: Forms
_Bootstrap’s form control styles, layout options, and custom components for creating a wide variety of forms._

Nota: Sila ke pautan ini untuk maklumat lanjut https://getbootstrap.com/docs/5.1/forms/

* Wujudkan fail **forms.html**. Dari Menu, klik **_File -> New File_**. Salin dan tampal kod berikut di antara dan simpan / (_**Save**_) fail

```
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Latihan 9 - Forms</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>
</head>
<body>
    <div class="container">
        <form>
            <div class="mb-3">
              <label for="exampleInputEmail1" class="form-label">Email address</label>
              <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
              <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div>
            </div>
            <div class="mb-3">
              <label for="exampleInputPassword1" class="form-label">Password</label>
              <input type="password" class="form-control" id="exampleInputPassword1">
            </div>
            <div class="mb-3 form-check">
              <input type="checkbox" class="form-check-input" id="exampleCheck1">
              <label class="form-check-label" for="exampleCheck1">Check me out</label>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</body>
</html>
```

* Untuk uji, **right-click** di fail **forms.html** dan pilih **Open with Live Server**

* Internet browser akan dibuka secara automatik ke pautan http://127.0.0.1:5500/forms.html. Sila rujuk paparan berikut

<img src="https://gitlab.com/akademi-cloud-connect/johor-ict/latihan-pembangunan-aplikasi-moden/uploads/4187773d77cdd00ae646cd80cf6f91db/image.png">

* Sahkan ujian dengan lihat perubahan laman web jika membesar dan mengecilkan saiz internet browser
